#pragma once
#include <chrono>
#include <string>
#include <boost/lexical_cast.hpp>

class Clock {
public:
    Clock() : init_(std::chrono::high_resolution_clock::now()) {};

    auto now() const {return std::chrono::high_resolution_clock::now();};
    auto elapsed() const;
    void reset() {init_ = std::chrono::high_resolution_clock::now();};

private:
    std::chrono::high_resolution_clock::time_point init_;
};


inline auto Clock::elapsed() const {
    auto elapsed = now() - init_;
    auto days = std::chrono::duration_cast<std::chrono::duration<int, std::ratio<86400>>>(elapsed).count();
    auto hours = std::chrono::duration_cast<std::chrono::hours>(elapsed).count() - days * 24;
    auto minutes = std::chrono::duration_cast<std::chrono::minutes>(elapsed).count() - days * 1440 - hours * 60;
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(elapsed).count() - days * 86400 - hours * 3600 - minutes * 60;
    auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() - days * 86400000 - hours * 3600000 - minutes * 60000 - seconds * 1000;

    std::string timestring("");
    if (days > 0)
        timestring.append(boost::lexical_cast<std::string>(days) + "d ");
    if (hours > 0)
        timestring.append(boost::lexical_cast<std::string>(hours) + "h ");
    if (minutes > 0)
        timestring.append(boost::lexical_cast<std::string>(minutes) + "min ");
    if (seconds > 0)
        timestring.append(boost::lexical_cast<std::string>(seconds) + "s ");
    if (milliseconds > 0)
        timestring.append(boost::lexical_cast<std::string>(milliseconds) + "ms ");
    timestring.pop_back();

    return timestring;
}
