#pragma once
#include <sstream>
#include <boost/any.hpp>
#include <boost/lexical_cast.hpp>


namespace boost {
    template<>
    inline bool lexical_cast<bool, std::string>(const std::string& arg) {
        std::istringstream stream(arg);
        bool b;
        stream >> std::boolalpha >> b;

        return b;
    }

    template<>
    inline std::string lexical_cast<std::string, bool>(const bool& b) {
        std::ostringstream stream;
        stream << std::boolalpha << b;

        return stream.str();
    }
}
