#pragma once
#include <string>
#include <unordered_map>
#include <boost/any.hpp>


template<typename K = std::string, typename V = boost::any>
class Map {
    std::unordered_map<K, V> map_;

public:
    typedef K key_type;
    typedef V value_type;

    Map() : map_() {}
    explicit Map(const size_t& size) : map_(size) {}

    auto& operator[](const key_type& key) {return map_[key];}
    template<typename T> const T as(const key_type& key) const {return boost::any_cast<T>(map_.at(key));}
    auto& at(const key_type& key) {return map_.at(key);}
    void clear() {map_.clear();}
    auto count(const key_type& key) const {return map_.count(key);}
    void emplace(const key_type& key, const value_type& value) {map_.emplace(key, value);}
    auto empty() const {return map_.empty();}
    auto size() const {return map_.size();}

    auto begin() -> decltype(map_.begin()) {return map_.begin();}
    auto end() -> decltype(map_.end()) {return map_.end();}
    auto begin() const -> decltype(map_.begin()) {return map_.begin();}
    auto end() const -> decltype(map_.end()) {return map_.end();}
};
