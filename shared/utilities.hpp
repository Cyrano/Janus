#pragma once
#include <cmath>
#include <map>
#include <numeric>
#include <string>
#include <vector>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Geometry>


namespace util {
    template<typename T>
    std::map<T, float> histogram(std::vector<T> data) {
        std::map<T, float> accumulator;
        for (const auto& value : data)
            ++accumulator[value];

        T total(0);
        for (const auto& pair : accumulator)
            total += pair.second;

        for (auto& pair : accumulator)
            pair.second /= total;

        return accumulator;
    }


    template<typename T>
    bool isZero(const T& num) {
        return (std::abs(num) < T(1e-5));
    }


    template<typename T>
    bool isFinite(const T& num) {
        return (std::abs(num) < T(1e7));
    }


    template<typename T>
    size_t digitsInNumber(const T& number) {
        std::string string = boost::lexical_cast<std::string>(number);
        return string.size();
    }


    template<typename T, typename S>
    std::vector<T> make(const S& container, const std::string& name) {
        std::vector<T> objects;
        for (auto& element : container)
            if (element.first.find(boost::to_upper_copy(name)) != std::string::npos) {
                S object_properties = boost::any_cast<S>(element.second);

                try {
                    for (int i = 0; i < boost::any_cast<int>(object_properties.at("number")); ++i)
                        objects.emplace_back(T(object_properties));
                }
                catch (const std::exception& e) {
                    std::cerr << "util::make\t'" << e.what() << "'\n";
                    throw;
                }
            }

        return objects;
    }


    template<typename T>
    T mean(const std::vector<T>& vector) {
        T sum = std::accumulate(vector.begin(), vector.end(), T(0));

        return sum / vector.size();
    }


    template<typename T>
    T product(const std::vector<T>& vector) {
        return std::accumulate(vector.begin(), vector.end(), T(1), [](auto& a, auto& b) {if (b != 0) return a * b; return a;});
    }


    template<typename T>
    T round(const T& number, const size_t& precision) {
        return std::floor(number * std::pow(10.0, precision) + 0.5) / std::pow(10.0, precision);
    }
}
