#pragma once
#include <random>
#include <Eigen/Geometry>


class Random {
    std::random_device init_;
    const float entropy_;
    const size_t seed_;
    std::uniform_real_distribution<float> distribution_;
    std::mt19937_64 engine_;

public:
    Random() : init_("/dev/random"), entropy_(init_.entropy()), seed_(init_()), distribution_(0.0, 1.0), engine_(seed_) {};
    Random(const Random& other);

    auto operator()() {return distribution_(engine_);}
    auto entropy() const {return entropy_;}
    auto seed() const {return seed_;}

    template<typename T = size_t> T index(const T& a) {std::uniform_int_distribution<T> distribution(0, a-1); return distribution(engine_);}
    template<typename T = int> T integer(const T& b) {std::uniform_int_distribution<T> distribution(0, b); return distribution(engine_);}
    template<typename T = int> T integer(const T& a, const T& b) {std::uniform_int_distribution<T> distribution(a, b); return distribution(engine_);}
    template<typename T = float> T real() {std::uniform_real_distribution<T> distribution(0.0, 1.0); return distribution(engine_);}
    template<typename T = float> T real(const T& b) {std::uniform_real_distribution<T> distribution(0.0, b); return distribution(engine_);}
    template<typename T = float> T real(const T& a, const T& b) {std::uniform_real_distribution<T> distribution(a, b); return distribution(engine_);}
    template<typename T = float> Eigen::Matrix<T, 3, 1> vector() {return vector<T>(-1.0, 1.0, 3);}
    template<typename T = float> Eigen::Matrix<T, 3, 1> vector(const int& dim) {return vector<T>(-1.0, 1.0, dim);}
    template<typename T = float> Eigen::Matrix<T, 3, 1> vector(const T& a, const T& b) {return vector<T>(a, b, 3);}
    template<typename T = float> Eigen::Matrix<T, 3, 1> vector(const T& a, const T& b, const int& dim);
    template<typename T = float> Eigen::Matrix<T, 3, 1> unitVector() {return vector<T>().normalized();}
    template<typename T = float> Eigen::Matrix<T, 3, 1> unitVector(const int& dim) {return vector<T>(dim).normalized();}
};


inline Random::Random(const Random& other) :
    init_("/dev/random"),
    entropy_(other.entropy_),
    seed_(other.seed_),
    distribution_(other.distribution_),
    engine_(seed_)
{
}


template<typename T = float>
Eigen::Matrix<T, 3, 1> Random::vector(const T& a, const T& b, const int& dim) {
    Eigen::Matrix<T, 3, 1> vec(Eigen::Matrix<T, 3, 1>::Zero());
    for (int i = 0; i < dim; ++i)
        vec[i] = real<T>(a, b);

    return vec;
}
