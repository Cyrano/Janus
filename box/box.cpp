#include "box.h"


Box::Box(const Options& options) try :
    dimensions_(options.at("box").as<size_t>("dimensions")),
    size_(options.at("box").as<std::vector<float>>("size").data()),
    temperature_(options.at("box").as<float>("temperature")),
    interaction_range_(options.at("potential").as<float>("range")),
    packing_correction_({std::sqrt(2.0), 1.0, 1.0}),
    potential_(Potential::make(options)),
    options_(options),
    vmd_offset_(0.009),
    vmd_precision_(8),
    vmd_radius_(20.0 / 3.0),
    vmd_scaling_(10.0),
    random_(),
    acceptance_(options.at("box")),
    cells_(options.at("box")),
    clusters_(),
    cluster_state_(0),
    delta_rotation_(temperature_ * PI * 2.0),
    delta_translation_(temperature_),
    densities_of_states_(3),
    energy_(0.0),
    histograms_(2),
    particles_(),
    particle_diameter_maximum_(0),
    particle_number_(0),
    step_(0),
    save_cluster_state_(0),
    save_cluster_number_(0),
    save_energy_(0.0),
    save_energy_particle_(0),
    save_index_(0),
    save_particle_(std::make_unique<Particle>("dummy"))
{
    if (dimensions_ == 1)
        delta_rotation_ = PI;

    populate_();
}
catch (const std::exception& e) {
    std::cerr << "Box::Box\t" << e.what() << "\n";
    throw;
}


Box::Box(const Box& other) :
    dimensions_(other.dimensions_),
    size_(other.size_),
    temperature_(other.temperature_),
    interaction_range_(other.interaction_range_),
    packing_correction_(other.packing_correction_),
    potential_(Potential::make(other.options_)),
    options_(other.options_),
    vmd_offset_(other.vmd_offset_),
    vmd_precision_(other.vmd_precision_),
    vmd_radius_(other.vmd_radius_),
    vmd_scaling_(other.vmd_scaling_),
    random_(other.random_),
    acceptance_(other.acceptance_),
    cells_(other.cells_),
    clusters_(other.clusters_),
    cluster_state_(other.cluster_state_),
    delta_rotation_(other.delta_rotation_),
    delta_translation_(other.delta_translation_),
    densities_of_states_(other.densities_of_states_),
    energy_(other.energy_),
    histograms_(other.histograms_),
    particles_(other.particles_),
    particle_diameter_maximum_(other.particle_diameter_maximum_),
    particle_number_(other.particle_number_),
    step_(other.step_),
    save_cluster_state_(other.save_cluster_state_),
    save_cluster_number_(other.save_cluster_number_),
    save_energy_(other.save_energy_),
    save_energy_particle_(other.save_energy_particle_),
    save_index_(other.save_index_),
    save_particle_(std::make_unique<Particle>(*other.save_particle_))
{
}


bool Box::acceptanceMetropolis_() {
    float metropolis = std::min(std::exp((save_energy_ - energy_) / temperature_), 1.0f);

    return (metropolis >= random_());
}


bool Box::acceptanceMetropolisRestricted_() {
    updateClusters();
    bool is_cluster_too_large = std::any_of(clusters_.begin(), clusters_.end(), [&](const auto& x) {return (x.size() > options_.maximum());});

    if (is_cluster_too_large)
        return false;

    float metropolis = std::min(std::exp((save_energy_ - energy_) / temperature_), 1.0f);

    return (metropolis >= random_());
}


bool Box::acceptanceWangLandau_() {
    if (energy_ >= INF)
        return false;

    float save_energy = util::round(save_energy_, 1);
    float energy = util::round(energy_, 1);
    float probability = std::exp(densities_of_states_[save_cluster_state_][save_energy] - densities_of_states_[cluster_state_][energy]);

    return (probability > random_());
}


std::vector<Eigen::Vector3f> Box::centeredPositions() const {
    std::vector<Eigen::Vector3f> positions_centered;
    positions_centered.reserve(particles_.size());

    Eigen::Vector3f displacement_vector = size_ / 2.0 - particles_[0].position();
    for (const auto& particle : particles_) {
        positions_centered.push_back(particle.position() + displacement_vector);
        for (size_t j = 0; j < dimensions_; ++j) {
            if (positions_centered.back()[j] >= size_[j])
                positions_centered.back()[j] -= size_[j];
            else if (positions_centered.back()[j] < 0.0f)
                positions_centered.back()[j] += size_[j];
        }
    }

    return positions_centered;
}


float Box::checkDensity_() const {
    float particle_radius = 0.0;
    float particle_space = 0.0;
    for (auto& particle : particles_) {
        particle_radius += particle.radius();
        particle_space += particle.space();
    }
    particle_radius /= particle_number_;

    float density = particle_space / space();
    float maximum_density = dimensions_ == 3 ? PI / std::sqrt(18) : 1.0;

    if (density > maximum_density)
        throw std::invalid_argument("volume density too high");

    return density;
}


void Box::findDiameterMaximum_() {
    for (const auto& particle : particles_)
        if (particle.diameter() > particle_diameter_maximum_)
            particle_diameter_maximum_ = particle.diameter();
}


void Box::loadState() {
    cluster_state_ = save_cluster_state_;
    energy_ = save_energy_;
    particles_[save_index_].position(save_particle_->position());
    particles_[save_index_].orientation(*save_particle_);
}


void Box::loadParticles(const std::vector<ParticleBase>& particles) {
    for (size_t i = 0; i < particles_.size(); ++i) {
        particles_[i].position(particles[i].position());
        particles_[i].orientation(particles[i].orientation());
    }
}


void Box::populate_() {
    particles_ = util::make<Particle>(options_.data(), "particle");
    particle_number_ = particles_.size();
    for (size_t i = 0; i < particles_.size(); ++i)
        particles_[i].id(i);

    if (options_.count("configuration")) {
        if (options_.file("configuration")->peek() == std::ifstream::traits_type::eof())
            throw std::invalid_argument("configuration file is empty");

        readGRO();
    }
    else if (options_.count("analyse"))
        findDiameterMaximum_();
    else if (options_.count("ordered"))
        populateClosePacked_();
    else
        populateBox_();

    updateEnergy();
}


void Box::populateBox_() {
    std::vector<Particle> particles_template = util::make<Particle>(options_.data(), "particle");

    bool is_setup_acceptable = false;
    if (options_.count("maximum"))
        for (size_t setup = 0; setup < 10 && !is_setup_acceptable; ++setup) {
            is_setup_acceptable = populateBoxSetup_(particles_template);

            if (is_setup_acceptable) {
                cells_.update(particles_, particle_diameter_maximum_ + interaction_range_);
                updateClusters();
                updateEnergy();

                is_setup_acceptable = std::none_of(clusters_.begin(), clusters_.end(), [&](const auto& x) {return (x.size() > options_.as<size_t>("maximum"));});
            }
        }
    else
        is_setup_acceptable = populateBoxSetup_(particles_template);

    if (!is_setup_acceptable)
        throw std::invalid_argument("particle density too high?");
}


bool Box::populateBoxSetup_(std::vector<Particle> particles_template) {
    float density = checkDensity_();
    size_t attempt_limit = (density < 0.4) ? 1e5 : particle_number_ * density * 1e4;
    HardSphere potential(*potential_);

    bool is_configuration_acceptable = false;
    for (size_t configuration = 0; configuration < 500 && !is_configuration_acceptable; ++configuration) {
        is_configuration_acceptable = true;
        particles_.clear();
        particles_.reserve(particle_number_);
        cells_.update(particles_);

        size_t attempt = 0;
        while (particles_.size() < particles_template.size() && is_configuration_acceptable) {
            particles_.emplace_back(particles_template[particles_.size()]);
            particles_.back().id(particles_.size()-1);

            bool is_particle_done = false;
            while (!is_particle_done && is_configuration_acceptable) {
                particles_.back().orientation(random_.unitVector(dimensions_));
                particles_.back().position(populateNeighbour_());
                particles_.back().remainBoxed(size_);
                cells_.update(particles_.back());

                float energy = potential.particle(particles_.back(), cells_);
                if (util::isZero(energy))
                    is_particle_done = true;

                if (attempt > attempt_limit)
                    is_configuration_acceptable = false;
                ++attempt;
            }
        }
    }

    findDiameterMaximum_();

    return is_configuration_acceptable;
}


void Box::populateClosePacked_() {
    float mean_radius = 0.0;
    for (const auto& particle : particles_)
        mean_radius += particle.radius();
    mean_radius /= particle_number_;

    size_t inflation_factor = 1;
    float size_maximum = size_.maxCoeff();
    std::vector<size_t> particles_per_dimension(3);
    std::vector<float> scaled_radius(3);
    while (util::product<size_t>(particles_per_dimension) < particle_number_) {
        size_t particle_maximum = util::product<size_t>(particles_per_dimension);

        for (size_t dim = 0; dim < dimensions_; ++dim) {
            particles_per_dimension[dim] = std::max(1.0f, std::floor(size_[dim] / size_maximum / packing_correction_[dim] * inflation_factor));

            bool is_number_odd = particles_per_dimension[dim] % 2 == 1;
            if (is_number_odd)
                ++particles_per_dimension[dim];
        }

        for (size_t dim = 0; dim < dimensions_; ++dim) {
            scaled_radius[dim] = mean_radius * size_[dim] / particles_per_dimension[dim] * 2.0 / packing_correction_[dim] / packing_correction_[dim];

            float minimum_radius = mean_radius * 2.0;
            if (options_.count("maximum"))
                minimum_radius += potential_->range() / 2.0;
            float hypotenuse = std::pow(minimum_radius, 2) - std::pow(scaled_radius[0], 2);
            hypotenuse = util::isFinite(hypotenuse) ? hypotenuse : 0;
            minimum_radius = dim == 0 ? minimum_radius / 2.0 : std::sqrt(hypotenuse);

            bool are_particles_too_close = scaled_radius[dim] < minimum_radius;
            if (are_particles_too_close)
                throw std::invalid_argument("maximum particle number is " + std::to_string(particle_maximum));
        }

        ++inflation_factor;
    }

    std::vector<size_t> counter(3, 0);
    for (size_t i = 0; i < particles_.size(); ++i) {
        Eigen::Vector3f position(Eigen::Vector3f::Zero());
        position[0] = scaled_radius[0] * (counter[0] + ((counter[1] + counter[2]) % 2 == 1) * 0.5) * 2.0;
        position[1] = scaled_radius[1] * counter[1];
        position[2] = scaled_radius[2] * counter[2];
        particles_[i].position(position);
        particles_[i].orientation(random_.unitVector(dimensions_));

        ++counter[0];
        if (counter[0] == particles_per_dimension[0]) {
            counter[0] = 0;
            ++counter[1];

            if (counter[1] == particles_per_dimension[1]) {
                counter[1] = 0;
                ++counter[2];
            }
        }
    }

    findDiameterMaximum_();

    cells_.update(particles_, mean_radius * 2.0 + interaction_range_);
    updateCells();
    updateClusters();
    updateEnergy();
}


Eigen::Vector3f Box::populateNeighbour_() {
    float minimum_distance = options_.count("maximum") ? 1.0 + interaction_range_ : 1.0;

    Eigen::Vector3f position = random_.vector(dimensions_).cwiseProduct(size_);
    if (options_.count("clustered") || options_.isWangLandau()) {
        if (particles_.size() > 1) {
            size_t random = random_.index(particles_.size()-1);
            Eigen::Vector3f shift = random_.unitVector(dimensions_) * particles_[random].diameter() * (random_.real(interaction_range_) + minimum_distance);
            position = particles_[random].position() + shift;
        }
        else
            position = size_ / 2.0;
    }

    return position;
}


void Box::rotateParticle(const size_t& index) {
    float angle = (dimensions_ > 1) ? random_.real(-delta_rotation_, delta_rotation_) : PI * ((random_() > 0.5) * 2 - 1);
    Eigen::Vector3f axis = (dimensions_ == 3) ? random_.unitVector(3) : Eigen::Vector3f::UnitZ();
    Eigen::AngleAxisf rotation = Eigen::AngleAxisf(angle, axis);
    particles_[index].rotate(rotation);
}


void Box::readGRO() {
    std::shared_ptr<std::fstream> file = options_.file("configuration");

    // find begin of first block
    std::string line;
    std::getline(*file, line);
    size_t timeline_first = boost::lexical_cast<size_t>(line.erase(0, 4));

    // find begin of second block and number of characters per block
    size_t timeline_second = 0;
    size_t characters_per_block = 0;
    bool is_timeline = false;
    while (file->good() && !is_timeline) {
        std::getline(*file, line);

        if (!line.empty() && line[0] == 't') {
            size_t delta_block = static_cast<size_t>(file->tellg());
            size_t delta_timeline = util::digitsInNumber(timeline_first) + line.size() + 1;
            characters_per_block = delta_block - delta_timeline;
            timeline_second = boost::lexical_cast<size_t>(line.erase(0, 4));

            is_timeline = true;
        }
    }

    // find end of file
    file->seekg(0, std::ios_base::end);
    size_t file_end = static_cast<size_t>(file->tellg());

    // find position of last data block
    size_t file_position = 0;
    size_t timeline = timeline_first;
    size_t timeline_delta = timeline_second - timeline_first;
    while (file_position < file_end) {
        file->seekg(file_position);
        file_position += characters_per_block + util::digitsInNumber(timeline);
        timeline += timeline_delta;
    }

    // skip first two lines
    std::getline(*file, line);
    std::getline(*file, line);

    // read particle positions and orientations
    std::string value;
    int value_length = 8;
    Eigen::Vector3f vector;
    for (size_t index = 0; index < particles_.size(); ++index) {
        std::getline(*file, line);
        for (int i = 0; i < 3; ++i) {
          value = line.substr(21 + value_length * i, 21 + value_length * (i+1));
          vector[i] = std::atof(value.c_str());
        }
        particles_[index].position(vector / vmd_scaling_);

        // read particle orientation from first patch
        std::getline(*file, line);
        for (int i = 0; i < 3; ++i) {
          value = line.substr(21 + value_length * i, 21 + value_length * (i+1));
          vector[i] = std::atof(value.c_str());
        }
        vector = vector / vmd_scaling_ - particles_[index].position();
        particles_[index].orientation(vector);

        // skip other patches
        for (size_t i = 1; i < particles_[index].patches().size(); i++)
            std::getline(*file, line);
    }

    findDiameterMaximum_();
}


void Box::resetHistograms() {
    for (auto& histogram : histograms_)
        for (auto& element : histogram)
            element.second = 0;
}


void Box::saveState(const size_t& index) {
    save_cluster_state_ = cluster_state_;
    save_cluster_number_ = clusters_.size();
    save_energy_particle_ = potential_->particle(particles_[index], cells_);
    save_energy_ = energy_;
    save_index_ = index;
    save_particle_ = std::make_unique<Particle>(particles_[index]);
}


float Box::space() const {
    float box_space = 1.0;
    for (size_t dim = 0; dim < dimensions_; ++dim)
        box_space *= size_[dim];

    return box_space;
}


float Box::particleSpace() const {
    float particle_space = 0.0;
    for (const auto& particle : particles_) {
        float exclusion_radius = particle.diameter() + potential_->range();
        switch (dimensions_) {
            case 1: particle_space += exclusion_radius;
                    break;
            case 2: particle_space += exclusion_radius * exclusion_radius * PI;
                    break;
            case 3: particle_space += exclusion_radius * exclusion_radius * exclusion_radius * PI * 4.0 / 3.0;
                    break;
        }
    }

    return particle_space;
}


void Box::translateParticle(const size_t& index) {
    particles_[index].translate(random_.vector(dimensions_) * delta_translation_);
    particles_[index].remainBoxed(size_);
}


void Box::translateAnywhere(const size_t& index) {
    particles_[index].position(random_.vector(0.0f, 1.0f, dimensions_).cwiseProduct(size_));
}


void Box::updateWangLandau(const float& increment) {
    float energy = util::round(energy_, 1);

    densities_of_states_[cluster_state_][energy] += increment;

    bool is_cluster_relevant = (cluster_state_ < 2);
    if (is_cluster_relevant)
        ++histograms_[cluster_state_][energy];
}


// write output file in Gromacs gro format, according to http://manual.gromacs.org/current/online/gro.html
void Box::writeGRO() const {
    std::ostringstream stream;

    // set format for digits
    stream << std::fixed << std::showpoint << std::setprecision(3);

    // title string (free format string, optional time in ps after 't=')
    stream << "t = " << step_ << "\n";

    // number of atoms (free format integer)
    size_t number_patches = 0;
    for (const auto& particle : particles_)
        number_patches += particle.patches().size();
    stream << std::setw(5) << particle_number_ + number_patches << "\n";

    // one line for each atom (fixed format)
    int counter_atom = 0;
    for (size_t counter_residue = 0; counter_residue < particle_number_; ++counter_residue) {
        // janus particle
               // residue number (5 positions, integer)
        stream << std::setw(5) << counter_residue+1
               // residue name (5 characters)
               << "JANUS"
               // atom name (5 characters)
               << std::setw(5) << particles_[counter_residue].name().substr(0, 5)
               // atom number (5 positions, integer)
               << std::setw(5) << counter_atom+1;
        // positions (x y z in 3 columns, each 8 positions with 3 decimal places; but more positions seem to work, too)
        for (size_t dim = 0; dim < 3; ++dim)
            stream << std::setw(vmd_precision_) << particles_[counter_residue].position()[dim] * vmd_scaling_;
        stream << "\n";

        ++counter_atom;

        // dummy atoms that represent the patches
        for (size_t counter_patch = 0; counter_patch < particles_[counter_residue].patches().size(); ++counter_patch) {
                   // residue number (5 positions, integer)
            stream << std::setw(5) << counter_residue+1
                   // residue name (5 characters)
                   << "PATCH"
                   // atom name (5 characters; 3 for particle name, 2 for patch name)
                   << std::setw(5) << particles_[counter_residue].name().substr(0, 3) + particles_[counter_residue].patch(counter_patch).name().substr(0, 2)
                   // atom number (5 positions, integer)
                   << std::setw(5) << counter_atom+1;
            // positions (x y z in 3 columns, each 8 positions with 3 decimal places)
            for (int dim = 0; dim < 3; dim++)
                stream << std::setw(vmd_precision_) << (particles_[counter_residue].position()[dim] + particles_[counter_residue].patch(counter_patch).orientation()[dim] * vmd_offset_) * vmd_scaling_;
            stream << "\n";

            ++counter_atom;
        }
    }

    // box vectors (free format, space separated reals, may not be 0 as VMD cannot display the box borders then)
    for (size_t dim = 0; dim < 3; ++dim)
        stream << std::setw(9) << (util::isZero(size_[dim]) ? 0.001 : size_[dim]) * vmd_scaling_;
    stream << "\n";

    // write to file
    *options_.file("trajectory") << stream.str() << std::flush;
}


void Box::writeNRG() const {
    *options_.file("energies") << step_ << "\t" << energy_ << std::endl;
}


// write vmd runcom file
void Box::writeVMD() const {
    std::ostringstream stream;
    stream << "# turn on lights\n"
           << "light 0 on\n"
           << "light 1 on\n"
           << "light 2 on\n"
           << "light 3 on\n"
           << "\n"
           << "# position the stage and axes\n"
           << "axes location off\n"
           << "stage location off\n"
           << "\n"
           << "# position and turn on menus\n"
           << "menu main     move 5   225\n"
           << "menu display  move 395 30\n"
           << "menu graphics move 395 500\n"
           << "menu color    move 125 225\n"
           << "menu files    move 125 325\n"
           << "menu labels   move 125 525\n"
           << "menu render   move 125 525\n"
           << "menu main     on\n"
           << "\n"
           << "# display settings\n"
           << "display resize 1680 1050\n"
           << "display projection orthographic\n"
           << "display rendermode GLSL\n"
           << "\n"
           << "# color definitions\n"
           << "color change rgb  0 0.07 0.20 0.48 ;# blue\n"
           << "color change rgb  1 0.70 0.20 0.10 ;# red\n"
           << "color change rgb  2 0.80 0.80 0.80 ;# grey\n"
           << "color change rgb  3 0.70 0.40 0.00 ;# orange\n"
           << "color change rgb  4 0.80 0.70 0.10 ;# yellow\n"
           << "color change rgb  7 0.13 0.47 0.04 ;# green\n"
           << "color change rgb  8 1.00 1.00 1.00 ;# white\n"
           << "color change rgb 10 0.10 0.70 0.80 ;# cyan\n"
           << "color change rgb 11 0.60 0.10 0.60 ;# purple\n"
           << "color change rgb 16 0.30 0.30 0.30 ;# black\n"
           << "\n"
           << "# invert display colors\n"
           << "color Display Background white\n"
           << "draw color black\n"
           << "\n"
           << "after idle {\n"
           << "  # set representations\n"
           << "  mol delrep 0 0\n"
           << "  material add Cartoon copy AOEdgy\n"
           << "  material change specular Cartoon 0.60\n"
           << "  material change shininess Cartoon 0.70\n"
           << "  material change outline Cartoon 2.00\n"
           << "  material change outlinewidth Cartoon 0.30\n\n";

    // set colour IDs
    std::vector<std::string> colours_particle {"16", "0", "7", "10", "6", "2"}; // black, blue, green, cyan, silver, grey
    std::vector<std::string> colours_patch {"1", "8", "3", "11", "7", "13", "14", "9"}; // red, white, orange, purple, mauve, ochre, pink

    // set representations of all particles
    size_t counter_particle = 0;
    size_t counter_patch = 0;
    size_t counter_rep = 0;
    std::string names_particle;
    for (const auto& particle : particles_) {
        bool is_represented = (names_particle.find(particle.name()) != std::string::npos);

        if (!is_represented) {
            // set particle as represented
            names_particle.append(particle.name());

            stream << "  mol selection \"name \\\"" << particle.name().substr(0, 5) << "\\\"\"\n"
                   << "  mol addrep 0" << "\n"
                   << "  mol modstyle " << counter_rep << " 0 VDW " << particle.radius() * vmd_radius_ * vmd_scaling_ << " 32" << "\n"
                   << "  mol modmaterial " << counter_rep << " 0 Cartoon" << "\n"
                   << "  mol modcolor " << counter_rep << " 0 \"ColorID\" " << colours_particle[counter_particle] << "\n"
                   // << "  mol smoothrep 0 " << counter_rep << " 10" << "\n"
                   << "\n";
            ++counter_particle;
            ++counter_rep;

            // set representations of all patches of current particle
            std::string names_patch;
            for (const auto& patch : particle.patches()) {
                bool is_represented = (names_patch.find(patch.name()) != std::string::npos);

                if (!is_represented) {
                    // set patch as represented
                    names_patch.append(patch.name());

                    // use cosine formula to calculate the dummy atom radius
                    float square_term = particle.radius() * particle.radius() + vmd_offset_ * vmd_offset_;
                    float cosine_term = particle.radius() * vmd_offset_ * std::cos(patch.size()) * 2.0;
                    float patch_size = std::sqrt(square_term - cosine_term);

                    stream << "  mol selection \"name \\\"" << particle.name().substr(0, 3) + patch.name().substr(0, 2) << "\\\"\"\n"
                           << "  mol addrep 0" << "\n"
                           << "  mol modstyle " << counter_rep << " 0 VDW " << patch_size * vmd_radius_ * vmd_scaling_ << " 32" << "\n"
                           << "  mol modmaterial " << counter_rep << " 0 Cartoon" << "\n"
                           << "  mol modcolor " << counter_rep << " 0 \"ColorID\" " << colours_patch[counter_patch] << "\n"
                           // << "  mol smoothrep 0 " << counter_rep << " 10" << "\n"
                           << "\n";

                    ++counter_patch;
                    ++counter_rep;
                }
            }
        }
    }

    stream << "  # draw simulation box\n"
           << "  pbc box_draw -color black\n"
           << "}\n";

    // write to file
    *options_.file("vmd") << stream.str() << std::flush;
}
