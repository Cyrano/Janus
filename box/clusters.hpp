#pragma once
#include <algorithm>
#include <vector>
#include "box/cells.hpp"
#include "box/cluster.hpp"
#include "box/potential.h"


class Clusters {
    std::vector<Cluster> clusters_;

    void find_(Particle& particle, const Cells& cells, const Potential& potential);
    void find_(const size_t& index, const std::vector<size_t>& particle_numbers, Cluster* pointer, std::vector<Particle>& particles, const Cells& cells, const Potential& potential);

public:
    Clusters() : clusters_() {};

    const auto begin() const {return clusters_.cbegin();}
    const auto end() const {return clusters_.cend();}

    auto& operator[](const size_t& index) {return clusters_.at(index);}
    void find(Particle& particle, std::vector<Particle>& particles, const Cells& cells, const Potential& potential);
    void find(std::vector<Particle>& particles, const Cells& cells, const Potential& potential);
    auto size() const {return clusters_.size();}
    size_t state() const;
    std::string list() const;
    std::string listShort() const;
};


inline std::string Clusters::list() const {
    std::vector<size_t> cluster_sizes;
    for (const auto& cluster : clusters_)
        cluster_sizes.push_back(cluster.size());
    std::sort(cluster_sizes.begin(), cluster_sizes.end(), std::greater<size_t>());
    // std::sort(cluster_sizes.begin(), cluster_sizes.end());

    std::string list;
    for (const auto& size : cluster_sizes)
        list += std::to_string(size) + ", ";
    list.resize(list.size()-2);

    return list;
}


inline std::string Clusters::listShort() const {
    std::vector<size_t> cluster_sizes;
    for (const auto& cluster : clusters_)
        cluster_sizes.push_back(cluster.size());
    std::sort(cluster_sizes.begin(), cluster_sizes.end(), std::greater<size_t>());

    std::string list;
    for (const auto& size : cluster_sizes)
        if (size > 1 || cluster_sizes.size() == 1)
            list += std::to_string(size) + ", ";

    bool is_populated = list.size() > 2;
    if (is_populated)
        list.resize(list.size()-2);
    else
        list = '1';

    return list;
}


inline size_t Clusters::state() const {
    bool is_clustered = (clusters_.size() == 1);
    if (is_clustered)
        return 1;

    bool is_unclustered = std::all_of(clusters_.begin(), clusters_.end(), [](const auto& x) {return (x.size() == 1);});
    if (is_unclustered)
        return 0;

    return 2;
}


inline void Clusters::find(Particle& particle, std::vector<Particle>& particles, const Cells& cells, const Potential& potential) {
    auto old_cluster = clusters_[particle.cluster()].particles();

    size_t old_cluster_id = clusters_[particle.cluster()].id();
    for (auto& cluster : clusters_)
        if (cluster.id() == old_cluster_id) {
            for (auto& id : old_cluster)
                particles[id].cluster(-1);
            cluster.clear();
        }

    for (const auto& cell : cells.neighbours(particle.cell()))
        for (const auto& neighbour : cells.particles(cell))
            if (potential.bonded(particle, neighbour))
                if (neighbour.get().cluster() != -1) {
                    if (std::none_of(old_cluster.begin(), old_cluster.end(), [&](const auto& x) {return neighbour.get().id() == particles[x].id();})) {
                        clusters_[neighbour.get().cluster()].emplace(particle.id());
                        particle.cluster(neighbour.get().cluster());
                    }
                }

    for (size_t i = 0; i < old_cluster.size(); ++i)
        if (particles[old_cluster[i]].cluster() == -1) {
            Cluster* pointer;
            bool found = false;
            for (size_t j = 0; j < clusters_.size(); ++j)
                if (clusters_[j].empty()) {
                    pointer = &clusters_[j];
                    found = true;
                    break;
                }

            if (!found) {
                clusters_.emplace_back(Cluster(clusters_.size()));
                pointer = &clusters_.back();
            }

            pointer->emplace(particles[old_cluster[i]].id());
            particles[old_cluster[i]].cluster(pointer->id());

            for (size_t j = i+1; j < old_cluster.size(); ++j)
                if (particles[old_cluster[j]].cluster() == -1)
                    if (potential.bonded(particles[old_cluster[i]], particles[old_cluster[j]])) {
                        pointer->emplace(particles[old_cluster[j]].id());
                        particles[old_cluster[j]].cluster(pointer->id());

                        find_(j, old_cluster, pointer, particles, cells, potential);
                    }
        }
}


inline void Clusters::find(std::vector<Particle>& particles, const Cells& cells, const Potential& potential) {
    /* Start with the first particle and assign it to a cluster. Then checks if there are any other particles
    * within a certain distance to this particle. If so, assign them to the same cluster and do the check for
    * this particle. If not, continue with the next particle.
    */

    clusters_.clear();
    for (auto& particle : particles)
        particle.cluster(-1);

    size_t cluster_counter = 0;
    for (auto& particle : particles)
        if (particle.cluster() == -1) {
            clusters_.emplace_back(Cluster(cluster_counter++));
            clusters_.back().emplace(particle.id());
            clusters_.back().neighbours()[particle.id()] = particle.id();
            particle.cluster(clusters_.back().id());

            for (const auto& cell : cells.neighbours(particle.cell()))
                for (const auto& neighbour : cells.particles(cell))
                    if (neighbour.get().cluster() == -1) {
                        if (potential.bonded(particle, neighbour)) {
                            clusters_.back().emplace(neighbour.get().id());
                            clusters_.back().neighbours()[neighbour.get().id()] = particle.id();
                            neighbour.get().cluster(clusters_.back().id());

                            find_(neighbour, cells, potential);
                        }
                    }
        }
}


inline void Clusters::find_(Particle& particle, const Cells& cells, const Potential& potential) {
    for (const auto& cell : cells.neighbours(particle.cell()))
        for (const auto& neighbour : cells.particles(cell)) {
            if (particle.id() == neighbour.get().id())
                continue;

            if (potential.bonded(particle, neighbour))
                if (neighbour.get().cluster() == -1) {
                    clusters_.back().emplace(neighbour.get().id());
                    clusters_.back().neighbours()[neighbour.get().id()] = particle.id();
                    neighbour.get().cluster(clusters_.back().id());

                    find_(neighbour, cells, potential);
                }
        }
}


inline void Clusters::find_(const size_t& index, const std::vector<size_t>& particle_numbers, Cluster* pointer, std::vector<Particle>& particles, const Cells& cells, const Potential& potential) {
for (size_t j = 0; j < particle_numbers.size(); ++j)
    if (particles[particle_numbers[j]].cluster() == -1)
        if (potential.bonded(particles[particle_numbers[index]], particles[particle_numbers[j]])) {
            pointer->emplace(particles[particle_numbers[j]].id());
            particles[particle_numbers[j]].cluster(pointer->id());

            find_(j, particle_numbers, pointer, particles, cells, potential);
        }
}
