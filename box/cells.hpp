#pragma once
#include <algorithm>
#include <cmath>
#include <exception>
#include <functional>
#include <vector>
#include "particle/particle.hpp"
#include "shared/map.hpp"
#include "shared/utilities.hpp"


class Cells {
    std::vector<float> box_size_;
    size_t dimensions_;
    std::vector<size_t> cell_number_;
    size_t cell_number_total_;
    std::vector<float> cell_size_;
    std::vector<std::vector<size_t>> neighbours_;
    std::vector<std::vector<std::reference_wrapper<Particle>>> particles_;

    size_t index_(const Particle& particle) const;
    size_t index_(const size_t& x, const size_t& y) const {return x + cell_number_[0] * y;}
    size_t index_(const size_t& x, const size_t& y, const size_t& z) const {return x + cell_number_[0] * y + cell_number_[0] * cell_number_[1] * z;}
    void setupCells_(std::vector<size_t>& cell_number, std::vector<float>& cell_size, const float& cell_size_minimum);
    void setupNeighbours_();
    void emplaceParticle_(Particle& particle);
    void eraseParticle_(const Particle& particle);
    size_t x_(const size_t& i, const int& dx) const;
    size_t y_(const size_t& i, const int& dx) const;
    size_t z_(const size_t& i, const int& dx) const;

public:
    explicit Cells(const Map<>& box);
    Cells(const Cells& other);


    void update(Particle& particle);
    void update(std::vector<Particle>& particles);
    void update(std::vector<Particle>& particles, const float& cell_size);

    const auto& neighbours() const {return neighbours_;}
    const auto& neighbours(const size_t& index) const {return neighbours_[index];}
    const auto& particles(const size_t& index) const {return particles_[index];}
    const auto& size() const {return cell_number_total_;}
};


inline Cells::Cells(const Map<>& box) try :
    box_size_(box.as<std::vector<float>>("size")),
    dimensions_(box.as<size_t>("dimensions")),
    cell_number_(3, 0),
    cell_number_total_(0),
    cell_size_(dimensions_, 0.0),
    neighbours_(0),
    particles_(0)
{
    setupCells_(cell_number_, cell_size_, 1.05f);
    neighbours_.resize(cell_number_total_);
    particles_.resize(cell_number_total_);
    setupNeighbours_();
}
catch (const std::exception& e) {
    std::cerr << "Cells::Cells\t" << e.what() << "\n";
    throw;
}


inline Cells::Cells(const Cells& other) :
    box_size_(other.box_size_),
    dimensions_(other.dimensions_),
    cell_number_(other.cell_number_),
    cell_number_total_(other.cell_number_total_),
    cell_size_(other.cell_size_),
    neighbours_(other.neighbours_),
    particles_(other.particles_)
{
}


inline size_t Cells::index_(const Particle& particle) const {
    std::vector<size_t> section(3, 0);
    for (size_t j = 0; j < dimensions_; ++j) {
        section[j] = std::floor(particle.position()[j] / cell_size_[j]);
        // take care of numerical noise
        section[j] = section[j] == cell_number_[j] ? 0 : section[j];
    }

    return index_(section[0], section[1], section[2]);
}


inline void Cells::setupCells_(std::vector<size_t>& cell_number, std::vector<float>& cell_size, const float& cell_size_minimum) {
    cell_number_total_ = 1;
    for (size_t i = 0; i < dimensions_; ++i) {
        size_t cells = static_cast<size_t>(std::floor(box_size_[i] / cell_size_minimum));
        cell_number[i] = std::min(static_cast<size_t>(10), std::max(cells, static_cast<size_t>(1)));
        cell_size[i] = box_size_[i] / cell_number[i];
        cell_number_total_ *= cell_number[i];
    }
}


inline void Cells::setupNeighbours_() {
    neighbours_ = std::vector<std::vector<size_t>>(cell_number_total_);

    std::vector<int> neighbour_max(cell_number_.size(), 2);
    for (size_t i = 0; i < cell_number_.size(); ++i)
        if (cell_number_[i] == 2)
            neighbour_max[i] = 1;

    switch (dimensions_) {
        case 1: {
            for (size_t i = 0; i < cell_number_total_; ++i)
                for (int dx = -1; dx < neighbour_max[0]; ++dx)
                    neighbours_[i].push_back(x_(i, dx));
            break;
        }
        case 2: {
            for (size_t i = 0; i < cell_number_total_; ++i)
                for (int dy = -1; dy < neighbour_max[1]; ++dy)
                    for (int dx = -1; dx < neighbour_max[0]; ++dx)
                        neighbours_[i].push_back(index_(x_(i, dx), y_(i, dy)));
            break;
        }
        case 3: {
            for (size_t i = 0; i < cell_number_total_; ++i)
                for (int dz = -1; dz < neighbour_max[2]; ++dz)
                    for (int dy = -1; dy < neighbour_max[1]; ++dy)
                        for (int dx = -1; dx < neighbour_max[0]; ++dx)
                            neighbours_[i].push_back(index_(x_(i, dx), y_(i, dy), z_(i, dz)));
            break;
        }
    }
}


inline size_t Cells::x_(const size_t& i, const int& dx) const {
    int x = (static_cast<int>(i) + dx) % static_cast<int>(cell_number_[0]);

    return (x < 0) ? x+cell_number_[0] : x;
}


inline size_t Cells::y_(const size_t& i, const int& dy) const {
    int y = (static_cast<int>(i / cell_number_[0]) + dy) % static_cast<int>(cell_number_[1]);

    return (y < 0) ? y+cell_number_[1] : y;
}


inline size_t Cells::z_(const size_t& i, const int& dz) const {
    int z = (static_cast<int>(i / (cell_number_[0] * cell_number_[1])) + dz) % static_cast<int>(cell_number_[2]);

    return (z < 0) ? z+cell_number_[2] : z;
}


inline void Cells::update(Particle& particle) {
    eraseParticle_(particle);
    emplaceParticle_(particle);
}


inline void Cells::update(std::vector<Particle>& particles) {
    particles_ = std::vector<std::vector<std::reference_wrapper<Particle>>>(cell_number_total_);
    for (auto& particle : particles)
        emplaceParticle_(particle);
}


inline void Cells::update(std::vector<Particle>& particles, const float& cell_size_minimum) {
    std::vector<float> cell_size(dimensions_);
    std::vector<size_t> cell_number(3);
    setupCells_(cell_number, cell_size, cell_size_minimum);

    bool is_update_needed = !std::equal(cell_size.begin(), cell_size.end(), cell_size_.begin());
    if (is_update_needed) {
        cell_size_ = cell_size;
        cell_number_ = cell_number;

        setupNeighbours_();
        update(particles);
    }
}


inline void Cells::emplaceParticle_(Particle& particle) {
    size_t index = index_(particle);
    particles_[index].push_back(particle);
    particle.cell(index);
}


inline void Cells::eraseParticle_(const Particle& particle) {
    auto& particles = particles_[particle.cell()];
    for (size_t i = 0; i < particles.size(); ++i)
        if (particle.id() == particles[i].get().id()) {
            particles.erase(particles.begin() + i);
            break;
        }
}
