#pragma once
#include <algorithm>
#include <map>
#include <vector>


class Cluster {
    size_t id_;
    std::vector<size_t> members_;
    std::map<size_t, size_t> neighbours_;

public:
    explicit Cluster(const size_t& id) : id_(id), members_(0), neighbours_() {};

    const auto begin() const {return members_.cbegin();}
    const auto end() const {return members_.cend();}

    void emplace(const size_t& index);
    void erase(const size_t& index);
    auto find(const size_t& index) const;
    auto sorted() const;

    void clear() {members_.clear();}
    auto empty() const {return members_.empty();}
    const auto& id() const {return id_;}
    std::string list() const;
    auto& neighbours() {return neighbours_;}
    const auto& neighbours() const {return neighbours_;}
    const auto& particles() const {return members_;}
    auto size() const {return members_.size();}
};


inline void Cluster::emplace(const size_t& index) {
    members_.emplace_back(index);
}


inline void Cluster::erase(const size_t& index) {
    for (size_t i = 0; i < members_.size(); ++i)
        if (index == members_[i]) {
            members_.erase(members_.begin() + i);
            break;
        }
}


inline auto Cluster::find(const size_t& index) const {
    for (auto& member : members_)
        if (member == index)
            return true;
    return false;
}


inline std::string Cluster::list() const {
    std::string list;
    for (const auto& member : members_)
        list += std::to_string(member) + ", ";

    if (!list.empty())
        list.resize(list.size()-2);

    return list;
}


inline auto Cluster::sorted() const {
    std::vector<size_t> members(members_);

    return std::sort(members.begin(), members.end());
}
