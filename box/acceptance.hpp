#pragma once
#include <algorithm>
#include <exception>
#include <iostream>
#include <memory>
#include <string>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/program_options.hpp>
#include <Eigen/Geometry>
#include "shared/constants.hpp"
#include "shared/random.hpp"
#include "shared/map.hpp"


class Acceptance {
    const size_t dimensions_;
    const Eigen::Vector3f box_size_;
    const float box_minimum_;
    size_t counter_acceptable_;
    size_t counter_rotation_;
    size_t counter_translation_;
    size_t counter_step_;
    float delta_minimum_;
    float deviation_;
    size_t interval_large_;
    size_t interval_small_;
    size_t interval_;
    float rate_rotation_;
    float rate_target_;
    float rate_translation_;
    float rotation_maximum_;
    Random random_;

    void updateDelta_(float& delta, const float& rate, const float& max);
    void updateRates_();

public:
    Acceptance(const Map<>& box);
    void update(float& delta_rotation, float& delta_translation);

    auto& counterRotation() {return counter_rotation_;}
    auto& counterTranslation() {return counter_translation_;}
    auto& counterStep() {return counter_step_;}
    const auto& interval() const {return interval_;}
};


inline Acceptance::Acceptance (const Map<>& box) try :
    dimensions_(box.as<size_t>("dimensions")),
    box_size_(box.as<std::vector<float>>("size").data()),
    box_minimum_(box_size_.head(dimensions_).minCoeff() / 3.0),
    counter_acceptable_(0),
    counter_rotation_(0),
    counter_translation_(0),
    counter_step_(0),
    delta_minimum_(1e-9),
    deviation_(0.1),
    interval_large_(5e5),
    interval_small_(5e4),
    interval_(interval_small_),
    rate_rotation_(1.0),
    rate_target_(0.33),
    rate_translation_(1.0),
    rotation_maximum_(PI * 2.0),
    random_()
{
}
catch (const std::exception& e) {
    std::cerr << "Acceptance::Acceptance\t" << e.what() << "\n";
    throw;
}


inline void Acceptance::updateDelta_(float& delta, const float& rate, const float& max) {
    delta *= 1.0 + rate - rate_target_;
    delta = std::min(delta, max);
    delta = std::max(delta, delta_minimum_);
}


inline void Acceptance::updateRates_() {
    rate_rotation_ = static_cast<float>(counter_rotation_) / std::min(interval_, counter_step_);
    rate_translation_ = static_cast<float>(counter_translation_) / std::min(interval_, counter_step_);
    bool is_rotation_acceptable = std::abs(rate_rotation_ - rate_target_) < deviation_;
    bool is_translation_acceptable = std::abs(rate_translation_ - rate_target_) < deviation_;
    bool is_rate_acceptable = dimensions_ > 1 ? (is_rotation_acceptable && is_translation_acceptable) : is_translation_acceptable;

    if (is_rate_acceptable)
        interval_ = interval_large_;
    else
        interval_ = interval_small_;
}


inline void Acceptance::update(float& delta_rotation, float& delta_translation) {
    updateRates_();

    if (dimensions_ > 1)
        updateDelta_(delta_rotation, rate_rotation_, rotation_maximum_);
    updateDelta_(delta_translation, rate_translation_, box_minimum_);

    counter_step_ = 0;
    counter_rotation_ = 0;
    counter_translation_ = 0;
}
