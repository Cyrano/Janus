#pragma once
#include <algorithm>
#include <cmath>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <vector>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/program_options.hpp>
#include <Eigen/Geometry>
#include "box/acceptance.hpp"
#include "box/cells.hpp"
#include "box/clusters.hpp"
#include "box/potential.h"
#include "interface/options.hpp"
#include "particle/particle.hpp"
#include "shared/random.hpp"
#include "shared/utilities.hpp"

namespace ac = boost::accumulators;


class Analysation;

class Box {
    friend Analysation;

protected:
    /* to be outsourced in future properties class */
    const size_t dimensions_;
    const Eigen::Vector3f size_;
    const float temperature_;
    const float interaction_range_;
    const std::vector<float> packing_correction_;
    const std::shared_ptr<Potential> potential_;
    const Options options_;
    const float vmd_offset_;
    const size_t vmd_precision_;
    const float vmd_radius_;
    const float vmd_scaling_;

    /* to be outsourced in future state class */
    Random random_;
    Acceptance acceptance_;
    Cells cells_;
    Clusters clusters_;
    size_t cluster_state_;
    float delta_rotation_;
    float delta_translation_;
    std::vector<std::map<float, double>> densities_of_states_;
    float energy_;
    std::vector<std::map<float, size_t>> histograms_;
    std::vector<Particle> particles_;
    float particle_diameter_maximum_;
    size_t particle_number_;
    size_t step_;

    /* to be outsourced in future save class */
    size_t save_cluster_state_;
    size_t save_cluster_number_;
    float save_energy_;
    float save_energy_particle_;
    size_t save_index_;
    std::unique_ptr<Particle> save_particle_;

    bool acceptanceMetropolis_();
    bool acceptanceMetropolisRestricted_();
    bool acceptanceWangLandau_();
    std::vector<Eigen::Vector3f> centeredPositions() const;
    float checkDensity_() const;
    void findDiameterMaximum_();
    void populate_();
    void populateBox_();
    bool populateBoxSetup_(std::vector<Particle> particles_template);
    void populateClosePacked_();
    Eigen::Vector3f populateNeighbour_();

public:
    Box(const Box& other);
    explicit Box(const Options& options);

    auto isAccepted() {return options_.isMonteCarlo() ? (options_.count("maximum") ? acceptanceMetropolisRestricted_() : acceptanceMetropolis_()) : acceptanceWangLandau_();}
    void loadState();
    void loadParticles(const std::vector<ParticleBase>& particles);
    void readGRO();
    void resetHistograms();
    void rotateParticle(const size_t& index);
    void saveState(const size_t& index);
    auto selectParticle() {return random_.index(particle_number_);}
    float space() const;
    float particleSpace() const;
    void translateParticle(const size_t& index);
    void translateAnywhere(const size_t& index);
    void updateAcceptance() {acceptance_.update(delta_rotation_, delta_translation_);}
    void updateCells() {cells_.update(particles_);}
    void updateCells(const size_t& index) {cells_.update(particles_[index]);}
    void updateClusters(const size_t& index) {clusters_.find(particles_[index], particles_, cells_, *potential_); cluster_state_ = clusters_.state();}
    void updateClusters() {clusters_.find(particles_, cells_, *potential_); cluster_state_ = clusters_.state();}
    void updateWangLandau(const float& increment);
    void updateEnergy() {energy_ = potential_->total(particles_);}
    void updateEnergy(const size_t& index) {energy_ += potential_->particle(particles_[index], cells_) - save_energy_particle_;}
    void writeNRG() const;
    void writeGRO() const;
    void writeVMD() const;

    /* to be outsourced in future box state class */
    const auto& clusters() {return clusters_;}
    void countRotation() {++acceptance_.counterRotation();}
    void countTranslation() {++acceptance_.counterTranslation();}
    void stepCounter(const size_t& step) {step_ = step; acceptance_.counterStep() = step;}
    const auto& acceptanceInterval() const {return acceptance_.interval();}
    const auto& energy() const {return energy_;}
    auto state() const {return clusters_.state();}
    auto& histograms() {return histograms_;}
    auto& densitiesOfStates() {return densities_of_states_;}
    const auto& particleNumber() const {return particle_number_;}
    const auto& temperature() const {return temperature_;}
};
