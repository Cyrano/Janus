#pragma once
#include <algorithm>
#include <cmath>
#include <exception>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <boost/any.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/lexical_cast.hpp>
#include <Eigen/Geometry>
#include <tbb/tbb.h>
#include <tbb/task_group.h>
#include "box/cells.hpp"
#include "interface/options.hpp"
#include "particle/particle.hpp"
#include "shared/constants.hpp"
#include "shared/map.hpp"

#include <iomanip>


class Potential {
protected:
    const std::string type_;
    const float attraction_;
    const Eigen::Vector3f box_;
    const Eigen::Vector3f box_half_;
    const int dimensions_;
    const bool heterophilic_;
    const float range_;
    const float repulsion_;
    float screening_;

    Potential(const Potential& other);
    explicit Potential(const Map<>& properties);

    template<typename T> float chargeInteraction_(const T& first, const T& second) const;
    float chargeAttractive_(const float& first, const float& second) const {return -attraction_ * first * second;}
    float chargeRepulsive_(const float& first, const float& second) const {return repulsion_ * first * second;}
    float exp(float arg) const {arg = 1.0f + arg / 1024.0f; return std::pow(arg, 1024);}
    virtual float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const = 0;
    virtual float particlePair_(const Particle& first, const Particle& second, bool& is_overlap) const;

public:
    bool bonded(const Particle& first, const Particle& second) const;
    Eigen::Vector3f distance(const Particle& first, const Particle& second) const;
    float total(const std::vector<Particle>& particle) const;
    float particle(const Particle& particle, const Cells& cells) const;
    static std::unique_ptr<Potential> make(const Options& options);

    virtual ~Potential() {};

    const auto& attraction() const {return attraction_;}
    const auto& range() const {return range_;}
    const auto& repulsion() const {return repulsion_;}
    const auto& screening() const {return screening_;}
};


class Doye : public Potential {
    float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const;
    float patchInteraction_(const Patch&, const Patch&, const float&, const float&, const float&) const;

public:
    explicit Doye(const Map<>& properties) : Potential(properties) {}
};


class ErdmannKroegerHess : public Potential {
    std::vector<int> exponent_one_;
    std::vector<int> exponent_two_;
    std::vector<int> exponent_three_;
    size_t number_tensors_;
    float normalisation_;
    Eigen::MatrixXd polynomial_coefficient_;
    size_t polynomial_degree_;
    size_t summation_bound_;

    float coefficientFit_(const size_t& parameter, const float& distance) const;
    float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const;
    void readPolynomials_(const Map<>& fit);
    void setExponents_();

public:
    explicit ErdmannKroegerHess(const Map<>& potential, const Map<>& fit);
};


class HardSphere : public Potential {
    float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const {return 0.0;}

public:
    template<typename T> HardSphere(const T& other) : T(other) {}

    explicit HardSphere(const Map<>& properties) : Potential(properties) {}
};


class KernFrenkel : public Potential {
    float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const;

public:
    explicit KernFrenkel(const Map<>& properties) : Potential(properties) {}
};


class KernFrenkelRing : public Potential {
    float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const;

public:
    explicit KernFrenkelRing(const Map<>& properties) : Potential(properties) {}
};


class StickyHardSphere : public Potential {
    float screening_inverse_;
    float normalisation_;

    float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const;

public:
    explicit StickyHardSphere(const Map<>& properties) : Potential(properties), screening_inverse_(1.0f / screening_), normalisation_(exp(screening_inverse_)) {}
};


class Water : public Potential {
    float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const;
    float particlePair_(const Particle& first, const Particle& second, bool& is_overlap) const;

public:
    explicit Water(const Map<>& properties) : Potential(properties) {}
};


class Yukawa : public Potential {
    const float factor_normation_;
    const size_t steps_integration_;

    float particleInteraction_(const Particle&, const Particle&, const Eigen::Vector3f&) const;
    float patchPair_(const Patch&, const Patch&, const Eigen::Quaternionf&, const Eigen::Vector3f, const Eigen::Vector3f&, const Eigen::Vector3f&) const;
    float integral_(const Eigen::Vector3f&, const Eigen::Vector3f&, const Eigen::Vector3f&, const Eigen::Quaternionf&, const float&, const float&) const;

public:
    explicit Yukawa(const Map<>& properties) : Potential(properties), factor_normation_(16.0 * PI * PI), steps_integration_(properties.as<size_t>("integration steps")) {}
};
