#include "potential.h"


Potential::Potential(const Map<>& properties) try :
    type_(properties.as<std::string>("type")),
    attraction_(properties.as<float>("attraction")),
    box_(properties.as<std::vector<float>>("box size").data()),
    box_half_(box_ / 2.0),
    dimensions_(properties.as<size_t>("dimensions")),
    heterophilic_(properties.as<bool>("heterophilic")),
    range_(properties.as<float>("range")),
    repulsion_(properties.as<float>("repulsion")),
    screening_(properties.as<float>("screening"))
{
}
catch (const std::exception& e) {
    std::cerr << "Potential::Potential\t" << e.what() << "\n";
    throw;
}


Potential::Potential(const Potential& other) :
    type_(other.type_),
    attraction_(other.attraction_),
    box_(other.box_),
    box_half_(other.box_half_),
    dimensions_(other.dimensions_),
    heterophilic_(other.heterophilic_),
    range_(other.range_),
    repulsion_(other.repulsion_),
    screening_(other.screening_)
{
}


std::unique_ptr<Potential> Potential::make(const Options& options) {
    std::unique_ptr<Potential> potential;
    std::string type = boost::to_lower_copy(options.at("potential").as<std::string>("type"));
    bool is_doye = (type == "do" || type == "doye");
    bool is_erdmann_kroeger_hess = (type == "ekh" || type == "erdmann-kröger-hess");
    bool is_hard_sphere = (type == "hs" || type == "hard sphere");
    bool is_kern_frenkel = (type == "kf" || type == "kern-frenkel");
    bool is_kern_frenkel_ring = (type == "kfr" || type == "kern-frenkel-ring");
    bool is_sticky_hard_sphere = (type == "shs" || type == "sticky hard sphere");
    bool is_water = (type == "wa" || type == "water");
    bool is_yukawa = (type == "yu" || type == "yukawa");

    if (is_doye)
        potential = std::make_unique<Doye>(options.at("potential"));
    else if (is_erdmann_kroeger_hess)
        potential = std::make_unique<ErdmannKroegerHess>(options.at("potential"), options.at("fit"));
    else if (is_hard_sphere)
        potential = std::make_unique<HardSphere>(options.at("potential"));
    else if (is_kern_frenkel)
        potential = std::make_unique<KernFrenkel>(options.at("potential"));
    else if (is_kern_frenkel_ring)
        potential = std::make_unique<KernFrenkelRing>(options.at("potential"));
    else if (is_sticky_hard_sphere)
        potential = std::make_unique<StickyHardSphere>(options.at("potential"));
    else if (is_water)
        potential = std::make_unique<Water>(options.at("potential"));
    else if (is_yukawa)
        potential = std::make_unique<Yukawa>(options.at("potential"));
    else
        throw std::invalid_argument("potential could not find '" + type + "'");

    return potential;
}


bool Potential::bonded(const Particle& first, const Particle& second) const {
    auto distance_vector = distance(first, second);
    float mean_diameter = first.radius() + second.radius();

    if (distance_vector.norm() < mean_diameter + range_)
        return true;
    return false;
}


template<typename T>
float Potential::chargeInteraction_(const T& first, const T& second) const {
    if (heterophilic_) {
        if (first.name() == second.name())
            return chargeRepulsive_(first.charge(), second.charge());
        else
            return chargeAttractive_(first.charge(), second.charge());
    }
    else {
        if (first.name() == second.name())
            return chargeAttractive_(first.charge(), second.charge());
        else
            return chargeRepulsive_(first.charge(), second.charge());
    }

    return 0.0;
}


Eigen::Vector3f Potential::distance(const Particle& first, const Particle& second) const {
    Eigen::Vector3f distance = second.position() - first.position();

    for (int i = 0; i < dimensions_; ++i) {
        if (distance[i] > box_half_[i])
            distance[i] -= box_[i];
        else if (distance[i] < -box_half_[i])
            distance[i] += box_[i];
    }

    return distance;
}


float Potential::total(const std::vector<Particle>& particle) const {
    float energy = 0.0;
    bool is_overlap = false;
    for (size_t i = 0; (i < particle.size() && !is_overlap); ++i)
        for(size_t j = i+1; (j < particle.size() && !is_overlap); ++j)
            energy += particlePair_(particle[i], particle[j], is_overlap);

    return energy;
}


float Potential::particle(const Particle& particle, const Cells& cells) const {
    float energy = 0.0;
    bool is_overlap = false;
    for (const auto& cell : cells.neighbours(particle.cell()))
        for (const auto& neighbour : cells.particles(cell))
            if (particle.id() != neighbour.get().id())
                energy += particlePair_(particle, neighbour, is_overlap);

    return energy;
}


float Potential::particlePair_(const Particle& first, const Particle& second, bool& is_overlap) const {
    auto distance_vector = distance(first, second);
    float distance = distance_vector.norm();
    float mean_diameter = first.radius() + second.radius();

    if (distance < mean_diameter) {
        is_overlap = true;
        return INF;
    }
    if (distance < mean_diameter + range_)
        return particleInteraction_(first, second, distance_vector);

    return 0.0;
}


float Doye::particleInteraction_(const Particle& first, const Particle& second, const Eigen::Vector3f& distance_vector) const {
    auto distance_unit = distance_vector.normalized();
    for (const auto& first_patch : first.patches()) {
        float dotproduct_first = std::max(std::min(first_patch.orientation().dot(distance_unit), 1.0f), -1.0f);
        bool is_first_aligned = dotproduct_first > first_patch.sizeCos();

        if (is_first_aligned) {
            for (const auto& second_patch : second.patches()) {
                float dotproduct_second = -std::max(std::min(second_patch.orientation().dot(distance_unit), 1.0f), -1.0f);
                bool is_second_aligned = dotproduct_second > second_patch.sizeCos();

                if (is_second_aligned) {
                    float radius = std::pow((first.radius() + second.radius()) / distance_vector.norm(), 6);
                    float angle_first = std::acos(dotproduct_first);
                    float angle_second = std::acos(dotproduct_second);

                    float energy = patchInteraction_(first_patch, second_patch, radius, angle_first, angle_second);
                    bool are_particles_parallel = first.orientation().dot(second.orientation()) > 0.0;
                    if (are_particles_parallel)
                        energy *= 1.3;

                    return energy;
                }
            }
        }
    }

    return 0.0;
}


float Doye::patchInteraction_(const Patch& first, const Patch& second, const float& radius, const float& angle_first, const float& angle_second) const {
    float radial = -chargeInteraction_(first, second) * (radius * radius - 2.0f * radius);
    float normalization = first.size() * second.size() * 2.0f;
    float angular_first = exp(-angle_first * angle_first / normalization);
    float angular_second = exp(-angle_second * angle_second / normalization);

    return radial * angular_first * angular_second;
}


ErdmannKroegerHess::ErdmannKroegerHess(const Map<>& potential, const Map<>& fit) try :
    Potential(potential),
    exponent_one_(0),
    exponent_two_(0),
    exponent_three_(0),
    number_tensors_(0),
    normalisation_(0),
    polynomial_coefficient_(0, 0),
    polynomial_degree_(3),
    summation_bound_(potential.as<size_t>("summation bound"))
{
    if (screening_ != fit.as<float>("screening"))
        std::cout << "warning: screening factor from parameters does not match fit\n";

    setExponents_();
    readPolynomials_(fit);

    normalisation_ = 26.05;
}
catch (const std::exception& e) {
    std::cerr << "ErdmannKroegerHess::ErdmannKroegerHess\t" << e.what() << "\n";
    throw;
}


float ErdmannKroegerHess::coefficientFit_(const size_t& index, const float& distance) const {
    float polynomial = 0.0;
    for (size_t i = 0; i < polynomial_degree_; ++i)
        polynomial += polynomial_coefficient_(index, i) * std::pow(distance, i);

    return std::exp(-distance / screening_) / std::pow(distance, 3) * polynomial;
}


float ErdmannKroegerHess::particleInteraction_(const Particle& first, const Particle& second, const Eigen::Vector3f& vector) const {
    auto normalisation = Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitZ(), first.orientation()).inverse();
    auto second_orientation = normalisation * second.orientation();
    auto distance_vector = normalisation * vector;
    float distance_norm = distance_vector.norm();

    float base_one = second_orientation.dot(distance_vector.normalized());
    float base_two = distance_vector[2] / distance_norm;
    float base_three = second_orientation[2];

    std::vector<float> tensor(number_tensors_);
    for (size_t i = 0; i < number_tensors_; ++i)
        tensor[i] = std::pow(base_one, exponent_one_[i]) * std::pow(base_two, exponent_two_[i]) * std::pow(base_three, exponent_three_[i])
                  + std::pow(base_one, exponent_two_[i]) * std::pow(base_two, exponent_one_[i]) * std::pow(base_three, exponent_three_[i]);

    float energy = 0.0;
    for (size_t i = 0; i < number_tensors_; ++i)
      energy += tensor[i] * coefficientFit_(i, distance_norm);

    return energy * normalisation_;
}


void ErdmannKroegerHess::readPolynomials_(const Map<>& fit) {
    number_tensors_ = exponent_one_.size();
    polynomial_coefficient_.resize(number_tensors_, polynomial_degree_);
    for (auto& tensor : fit)
        if (tensor.first.find("tensor") != std::string::npos) {
            std::vector<float> coefficient = boost::any_cast<std::vector<float>>(tensor.second);
            size_t index = boost::lexical_cast<size_t>(boost::replace_all_copy(tensor.first, "tensor ", "")) - 1;

            if (index > number_tensors_)
                throw std::invalid_argument("wrong number of tensors");

            for (size_t i = 0; i < polynomial_degree_; ++i)
                polynomial_coefficient_(index, i) = coefficient[i];
        }
}


void ErdmannKroegerHess::setExponents_() {
    for (size_t i = 0; i <= summation_bound_; ++i) {
        for (size_t j = 0; j <= summation_bound_; ++j) {
            if ((i + j) % 2 == 0)
              continue;

            for (size_t k = j; k <= summation_bound_; k = k + 2) {
                exponent_one_.push_back(j);
                exponent_two_.push_back(k);
                exponent_three_.push_back(i);
            }
        }
    }
}


float KernFrenkel::particleInteraction_(const Particle& first, const Particle& second, const Eigen::Vector3f& distance_vector) const {
    auto distance_unit = distance_vector.normalized();
    for (const auto& first_patch : first.patches()) {
        float dotproduct_first = first_patch.orientation().dot(distance_unit);
        bool is_first_aligned = dotproduct_first > first_patch.sizeCos();

        if (is_first_aligned) {
            for (const auto& second_patch : second.patches()) {
                float dotproduct_second = -second_patch.orientation().dot(distance_unit);
                bool is_second_aligned = dotproduct_second > second_patch.sizeCos();

                if (is_first_aligned && is_second_aligned)
                    return chargeInteraction_(first_patch, second_patch);
            }
        }
    }

    return 0.0;
}


float KernFrenkelRing::particleInteraction_(const Particle& first, const Particle& second, const Eigen::Vector3f& distance_vector) const {
    auto distance_unit = distance_vector.normalized();
    float dotproduct_first = first.orientation().dot(distance_unit);
    float dotproduct_second = -second.orientation().dot(distance_unit);
    float factor_orientation = 1.1;

    for (const auto& first_ring : first.rings()) {
        bool is_first_aligned = dotproduct_first > first_ring.min() && dotproduct_first < first_ring.max();
        if (is_first_aligned)

            for (const auto& second_ring : second.rings()) {
                bool is_second_aligned = dotproduct_second > second_ring.min() && dotproduct_second < second_ring.max();


                if (is_second_aligned) {
                    float potential = chargeInteraction_(first_ring, second_ring);
                    if (first.orientation().dot(second.orientation()) > 0.0)
                        potential *= factor_orientation;

                    return potential / factor_orientation;
                }
            }
    }

    return 0.0;
}


float StickyHardSphere::particleInteraction_(const Particle& first, const Particle& second, const Eigen::Vector3f& distance_vector) const {
    float distance = distance_vector.norm();
    return chargeInteraction_(first.patch(0), second.patch(0)) * exp(-distance * screening_inverse_) / distance * normalisation_;
}


float Water::particleInteraction_(const Particle& first, const Particle& second, const Eigen::Vector3f& distance_vector) const {
    auto distance_unit = distance_vector.normalized();
    for (const auto& first_patch : first.patches()) {
        float dotproduct_first = first_patch.orientation().dot(distance_unit);
        bool is_first_aligned = dotproduct_first > first_patch.sizeCos();

        if (is_first_aligned) {
            for (const auto& second_patch : second.patches()) {
                float dotproduct_second = -second_patch.orientation().dot(distance_unit);
                bool is_second_aligned = dotproduct_second > second_patch.sizeCos();

                if (is_first_aligned && is_second_aligned)
                    return chargeInteraction_(first_patch, second_patch);
            }
        }
    }

    return 0.0;
}


float Water::particlePair_(const Particle& first, const Particle& second, bool& is_overlap) const {
    auto distance_vector = distance(first, second);
    float distance = distance_vector.norm();
    float mean_diameter = first.radius() + second.radius();

    if (distance < mean_diameter) {
        is_overlap = true;
        return INF;
    }
    if (distance < mean_diameter + range_ / 2.0)
        return 0.0;
    if (distance < mean_diameter + range_)
        return particleInteraction_(first, second, distance_vector);

    return 0.0;
}


float Yukawa::particleInteraction_(const Particle& first, const Particle& second, const Eigen::Vector3f& distance_vector) const {
    Eigen::Quaternionf quaternion_centring = Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitZ(), first.orientation()).inverse();
    Eigen::Vector3f orientation_second = Eigen::Vector3f::UnitZ() * second.radius();

    float potential = 0.0;
    for (const auto& first_patch : first.patches())
        for (const auto& second_patch : first.patches()) {
            Eigen::Vector3f orientation_first = quaternion_centring * first.orientation() * first.radius();
            std::cout << first.id() << "\t" << second.id() << "\t";
            potential += patchPair_(first_patch, second_patch, quaternion_centring, quaternion_centring * distance_vector, orientation_first, orientation_second);
        }

    return potential;
}


float Yukawa::patchPair_(const Patch& first, const Patch& second, const Eigen::Quaternionf& quaternion_centring, const Eigen::Vector3f distance_vector, const Eigen::Vector3f& orientation_first, const Eigen::Vector3f& orientation_second) const {
    Eigen::Quaternionf rotation_second = quaternion_centring * Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitZ(), second.orientation());
    float delta_theta_first = first.size() / steps_integration_;
    float delta_theta_second = second.size() / steps_integration_;
    float charge_interaction = chargeInteraction_(first, second);

    std::cout << first.name() << second.name() << "\t";
    float integral = integral_(distance_vector, orientation_first, orientation_second, rotation_second, delta_theta_first, delta_theta_second);
    std::cout << charge_interaction * integral / factor_normation_ << "\n";

    return charge_interaction * integral / factor_normation_;
}


float Yukawa::integral_(const Eigen::Vector3f& distance_vector, const Eigen::Vector3f& orientation_first, const Eigen::Vector3f& orientation_second, const Eigen::Quaternionf& rotation_second, const float& delta_theta_first, const float& delta_theta_second) const {
    float integral = 0.0;
    for (size_t i = 0; i < steps_integration_; ++i) {
        float theta_first = delta_theta_first * (i + 0.5);
        float theta_first_sin = std::sin(theta_first);
        Eigen::AngleAxisf rotation_theta_first(theta_first, Eigen::Vector3f::UnitY());
        size_t phi_first_steps = std::ceil(std::sin(theta_first) * steps_integration_);
        float delta_phi_first = PI * 2.0 / phi_first_steps;

        for (size_t j = 0; j < phi_first_steps; ++j) {
            float phi_first = delta_phi_first * (j + 0.5);
            Eigen::Quaternionf rotation_first_surface = Eigen::AngleAxisf(phi_first, Eigen::Vector3f::UnitZ()) * rotation_theta_first;
            Eigen::Vector3f orientation_first_surface = rotation_first_surface * orientation_first;

            for (size_t m = 0; m < steps_integration_; ++m) {
                float theta_second = delta_theta_second * (m + 0.5);
                float theta_second_sin = std::sin(theta_second);
                Eigen::AngleAxisf rotation_theta_second(theta_second, Eigen::Vector3f::UnitY());
                size_t steps_phi_second = std::ceil(std::sin(theta_second) * steps_integration_);
                float delta_phi_second = PI * 2.0 / steps_phi_second;
                float factor_integration = delta_theta_first * delta_phi_first * delta_theta_second * delta_phi_second;

                for (size_t n = 0; n < steps_phi_second; ++n) {
                    float phi_second = delta_phi_second * (n + 0.5);
                    Eigen::Quaternionf rotation_second_surface = Eigen::AngleAxisf(phi_second, Eigen::Vector3f::UnitZ()) * rotation_theta_second;
                    Eigen::Vector3f orientation_second_surface = rotation_second * rotation_second_surface * orientation_second;
                    float surface_distance = (distance_vector + orientation_second_surface - orientation_first_surface).norm();
                    float factor_screening = exp(-surface_distance / screening_);

                    // std::cout << std::setw(8) << theta_first << "\t" << std::setw(8) << phi:second << "\t"
                    //           << std::setw(8) << theta_second << "\t" << std::setw(8) << phi_second << "\t"
                    //           << std::setw(8) << factor_integration * factor_screening * theta_first_sin * theta_second_sin / surface_distance << "\n";

                    integral += factor_integration * factor_screening * theta_first_sin * theta_second_sin / surface_distance;
                }
            }
        }
    }

    return integral;
}
