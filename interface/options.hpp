#pragma once
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <memory>
#include <boost/version.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <Eigen/Geometry>
#include "interface/parser.hpp"
#include "shared/map.hpp"

namespace fs = boost::filesystem;
namespace po = boost::program_options;


class Options {
    size_t cluster_maximum_;
    std::string distribution_;
    std::string file_stem_;
    std::map<std::string, std::shared_ptr<std::fstream>> files_;
    po::variables_map map_;
    po::options_description options_allowed_;
    po::options_description options_files_;
    po::options_description options_necessary_;
    po::positional_options_description options_positional_;
    Parser<Map<>> parsed_;
    std::string type_;
    std::string versionBoost_;
    std::string versionEigen_;

    std::string filename_(std::string key, std::string extension);
    void openfiles_();

public:
    Options(int argc, char** argv);
    Options(const Options& other);

    template <typename T> T as(const std::string& key) const {return map_.at(key).as<T>();}
    auto at(const std::string& key) const {return parsed_.at(key);}
    void checkFile_(const std::string& filename) const;
    auto count(const std::string& key) const {return map_.count(key);}
    const std::shared_ptr<std::fstream>& file(const std::string& name) const;
    auto fileStem() const {return file_stem_;}
    void help() const;
    auto isMonteCarlo() const {return (type_ == "monte-carlo" || type_ == "mc");}
    auto isWangLandau() const {return (type_ == "wang-landau" || type_ == "wl");}
    const auto& maximum() const {return cluster_maximum_;}
    const auto& data() const {return parsed_.data();}
    const auto& type() const {return type_;}
    const auto& distribution() const {return distribution_;}
};


inline Options::Options (int argc, char** argv) try :
    cluster_maximum_(0),
    distribution_(),
    file_stem_(),
    files_(),
    map_(),
    options_allowed_("Allowed options"),
    options_files_("Optional files"),
    options_necessary_("Necessary files"),
    options_positional_(),
    parsed_(),
    type_("void"),
    versionBoost_(std::to_string(BOOST_VERSION / 100000) + "." + std::to_string(BOOST_VERSION / 100 % 1000) + "." + std::to_string(BOOST_VERSION % 100)),
    versionEigen_(std::to_string(EIGEN_WORLD_VERSION) + "." + std::to_string(EIGEN_MAJOR_VERSION) + "." + std::to_string(EIGEN_MINOR_VERSION))
{
    /* Answering the Question: What are Possible Options? */

    options_allowed_.add_options()
        ("help,h", "show this help")
        ("analyse,a", po::value<size_t>() -> implicit_value(10000), "analyse a trajectory at n points")
        // ("integrate,i", "perform a thermodynamic integration")
        // ("resume,r", "resume a simulation")
        ("maximum,m", po::value<size_t>(), "set a limit to the cluster sizes")
        ("type,t", po::value<std::string>() -> default_value("monte-carlo"), "set type of simulation: Monte-Carlo or Wang-Landau")
        ("distribution,d", po::value<std::string>(), "obtain only a distribution from MC simulation: clusters or states")
        ("clustered", "use clustered starting configuration")
        ("ordered", "use ordered starting configuration")
        ("unrecorded", "disable trajectory")
        ("progress", "display progress during run")
        ("vtk", "write vtk file");

    options_necessary_.add_options()
        ("parameters", po::value<std::string>() -> required(), "parameter file to set up the system");

    options_files_.add_options()
        ("configuration", po::value<std::string>(), "coordinate file for starting configuration")
        ("energies", po::value<std::string>(), "energy file to save the energy of the system")
        ("fit", po::value<std::string>(), "fit parameter file to set up the potential")
        ("trajectory", po::value<std::string>(), "coordinate file to save the trajectory of the system")
        ("vmd", po::value<std::string>(), "VMD parameter file for beautified visualisation");

    options_positional_.add("parameters", 1);

    po::options_description combined;
    combined.add(options_allowed_).add(options_necessary_).add(options_files_);
    po::store(po::command_line_parser(argc, argv).options(combined).positional(options_positional_).run(), map_);

    if (map_.count("help")) {
        help();
        std::exit(EXIT_SUCCESS);
    }

    if (map_.count("maximum"))
        cluster_maximum_ = map_.at("maximum").as<size_t>();

    if (map_.count("cluster"))
        map_.insert(std::make_pair("unrecorded", po::variable_value()));

    po::notify(map_);
    openfiles_();
}
catch (std::exception& e) {
    std::cerr << "Options::options\t" << e.what() << "\n";
    throw;
}


inline Options::Options (const Options& other) :
    cluster_maximum_(other.cluster_maximum_),
    distribution_(other.distribution_),
    file_stem_(other.file_stem_),
    files_(other.files_),
    map_(other.map_),
    options_allowed_(other.options_allowed_),
    options_files_(other.options_files_),
    options_necessary_(other.options_necessary_),
    options_positional_(other.options_positional_),
    parsed_(other.parsed_),
    type_(other.type_),
    versionBoost_(other.versionBoost_),
    versionEigen_(other.versionEigen_)
{
}


inline const std::shared_ptr<std::fstream>& Options::file(const std::string& name) const {
    try {
        return files_.at(name);
    }
    catch (const std::exception& e) {
        std::cerr << "Options::file\t" << "file '" << name << "' not found\n";
        throw;
    }
}


inline std::string Options::filename_(std::string key, std::string extension) {
    if (map_.count(key))
        return map_.at(key).as<std::string>();

    return file_stem_ + "." + extension;
}


inline void Options::checkFile_(const std::string& filename) const {
    if (!fs::exists(filename))
        throw std::invalid_argument("file " + filename + " not found");
}


inline void Options::openfiles_() {
    try {
        // open parameter file
        file_stem_ = fs::path(map_.at("parameters").as<std::string>()).stem().string();
        checkFile_(filename_("parameters", "par"));
        files_.emplace("parameters", std::make_shared<std::fstream>(filename_("parameters", "par"), std::fstream::in));

        type_ = map_.at("type").as<std::string>();
        if (!isMonteCarlo() && !isWangLandau())
            throw std::invalid_argument("wrong simulation type '" + type_ + "'");

        if (map_.count("distribution")) {
            distribution_ = map_.at("distribution").as<std::string>();
            if (distribution_ != "clusters" && distribution_ != "states")
                throw std::invalid_argument("wrong distribution type '" + distribution_ + "'");
        }
        parsed_ = Parser<Map<>>(files_.at("parameters"));

        // open files for Monte Carlo simulation
        if (isMonteCarlo() && !map_.count("analyse") && !map_.count("unrecorded")) {
            files_.emplace("energies", std::make_shared<std::fstream>(filename_("energies", "dat"), std::fstream::out));
            files_.emplace("trajectory", std::make_shared<std::fstream>(filename_("trajectory", "gro"), std::fstream::out));
            files_.emplace("vmd", std::make_shared<std::fstream>(filename_("vmd", "vmdrc"), std::fstream::out));
        }

        // open files for analysation
        if (map_.count("analyse")) {
            files_.emplace("analyse", std::make_shared<std::fstream>(file_stem_ + ".analyse", std::fstream::out));
            files_.emplace("clusters", std::make_shared<std::fstream>(file_stem_ + ".analyse.clusters", std::fstream::out));

            if (map_.count("vtk"))
                files_.emplace("vtk", std::make_shared<std::fstream>(file_stem_ + ".analyse.vtk", std::fstream::out));
        }

        // open configuration file if needed
        bool is_configuration_needed = (map_.count("resume") || map_.count("analyse"));
        if (map_.count("configuration")) {
            checkFile_(filename_("parameters", "par"));
            files_.emplace("configuration", std::make_shared<std::fstream>(map_["configuration"].as<std::string>(), std::fstream::in));
        }
        else if (is_configuration_needed) {
            bool is_default_available = fs::exists(file_stem_ + ".gro");
            if (is_default_available)
                files_.emplace("configuration", std::make_shared<std::fstream>(file_stem_ + ".gro", std::fstream::in));
            else
                throw std::invalid_argument("no configuration file given");
        }

        // open file for the fit of the Erdmann-Kröger-Hess potential
        std::string potential_type = parsed_.at("potential").as<std::string>("type");
        bool is_erdmann_kroeger_hess = (potential_type == "ekh" || potential_type == "erdmann-kröger-hess");
        if (is_erdmann_kroeger_hess)
            files_.emplace("fit", std::make_shared<std::fstream>(filename_("fit", "fit"), std::fstream::in | std::fstream::out | std::fstream::app));
    }
    catch (std::exception& e) {
        std::cerr << "Options::openfiles\t" << e.what() << "\n";
        throw;
    }
}


inline void Options::help() const {
    std::cout << "Janus, using Eigen " << versionEigen_ << " and Boost " << versionBoost_ << " libraries.\n\n"
              << "Usage: janus [options] [files] parameters.par\n\n"
              << options_allowed_ << "\n"
              << options_necessary_ << "\n"
              << options_files_ << std::endl;
}
