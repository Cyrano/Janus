#pragma once
#include <exception>
#include <fstream>
#include <locale>
#include <string>
#include <type_traits>
#include <vector>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>
#include "shared/specialisations.hpp"
#include "shared/typeswitch.hpp"


template<typename T>
class Parser {
    T data_;

    boost::any cast_(const std::string& token) const;
    bool getTokens_(const std::shared_ptr<std::fstream>& file, std::vector<std::string>& tokens);
    bool isBool_(const std::string& token) const;
    bool isFloat_(const std::string& token) const;
    bool isInteger_(const std::string& token) const;
    void sanitiseMap_(T& map) const;

public:
    Parser() : data_() {};
    explicit Parser(const std::shared_ptr<std::fstream>& file);
    Parser(const Parser& other) : data_(other.data_) {};

    T at(const std::string& name) const;
    const auto& data() const {return data_;};
    auto size() const {return data_.size();};
};


template<typename T>
Parser<T>::Parser(const std::shared_ptr<std::fstream>& file) : data_(1) {
    static_assert(std::is_same<std::string, typename T::key_type>::value, "key has to be of type std::string");

    file->seekg(0);
    T map_properties;
    T map_interaction;
    auto collector = &map_properties;
    std::vector<std::string> token;

    try {
        while(getTokens_(file, token)) {
            bool is_comment = token[0].find("//") != std::string::npos;
            if (is_comment)
                continue;

            bool is_newline = token[0].empty();
            bool is_patch = token[0].find("PATCH") != std::string::npos;
            bool is_ring = token[0].find("RING") != std::string::npos;
            bool is_interaction = is_patch || is_ring;
            if ((is_newline || is_interaction) && !map_interaction.empty()) {
                std::string name = boost::any_cast<std::string>(map_interaction.at("name"));
                sanitiseMap_(map_interaction);
                map_properties.emplace(name, map_interaction);
                map_interaction.clear();
            }

            if (is_interaction)
                collector = &map_interaction;

            if (is_newline) {
                if (map_properties.count("name")) {
                    data_.emplace(boost::any_cast<std::string>(map_properties.at("name")), map_properties);
                    map_properties.clear();
                    collector = &map_properties;
                }
            }
            else if (token.size() == 1){
                collector->emplace("name", boost::to_upper_copy(token[0]));
            }
            else if (token.size() == 2)
                collector->emplace(token[0], cast_(token[1]));
        }

        // emplace any leftover data
        if (map_interaction.count("name")) {
            std::string name = boost::any_cast<std::string>(map_interaction.at("name"));
            sanitiseMap_(map_interaction);
            map_properties.emplace(name, map_interaction);
        }
        if (map_properties.count("name"))
            data_.emplace(boost::any_cast<std::string>(map_properties.at("name")), map_properties);

        // assert correct types of input data
        size_t dimensions;
        std::vector<float> box_size;
        for (auto& block : data_)
            if (block.first == "BOX") {
                T map = boost::any_cast<T>(block.second);
                box_size = type::switch_(map.at("size")) (
                    type::case_<float>([](auto d) {return std::vector<float>(1, d);}),
                    type::case_<std::vector<float>>([](auto v) {return v;}),
                    type::default_([] {throw std::invalid_argument("invalid box size"); return std::vector<float>();}));
                dimensions = box_size.size();
                box_size.resize(3, 0);

                map.at("size") = box_size;
                map.emplace("dimensions", dimensions);
                block.second = map;
            }
        for (auto& block : data_)
            if (block.first.find("PARTICLE") != std::string::npos) {
                T map = boost::any_cast<T>(block.second);
                map.emplace("dimensions", dimensions);
                block.second = map;
            }
        for (auto& block : data_)
            if (block.first == "POTENTIAL") {
                T map = boost::any_cast<T>(block.second);
                map.emplace("box size", box_size);
                map.emplace("dimensions", dimensions);

                if (map.count("integration steps"))
                    map.at("integration steps") = type::switch_(map.at("integration steps")) (
                        type::case_<int>([](auto i) {return boost::lexical_cast<size_t>(i);}),
                        type::case_<float>([](auto d) {return boost::lexical_cast<size_t>(d);}),
                        type::default_([] {return 0;}));

                block.second = map;
            }
        for (auto& block : data_)
            if (block.first == "SAMPLING") {
                T map = boost::any_cast<T>(block.second);
                map.at("surface steps") = type::switch_(map.at("surface steps")) (
                    type::case_<int>([](auto i) {return boost::lexical_cast<size_t>(i);}),
                    type::case_<float>([](auto d) {return boost::lexical_cast<size_t>(d);}),
                    type::default_([] {return 0;}));
                map.at("angle steps") = type::switch_(map.at("angle steps")) (
                    type::case_<int>([](auto i) {return boost::lexical_cast<size_t>(i);}),
                    type::case_<float>([](auto d) {return boost::lexical_cast<size_t>(d);}),
                    type::default_([] {return 0;}));
                map.at("distance steps") = type::switch_(map.at("distance steps")) (
                    type::case_<int>([](auto i) {return boost::lexical_cast<size_t>(i);}),
                    type::case_<float>([](auto d) {return boost::lexical_cast<size_t>(d);}),
                    type::default_([] {return 0;}));
                block.second = map;
            }
        for (auto& block : data_)
            if (block.first == "SIMULATION") {
                T map = boost::any_cast<T>(block.second);
                map.at("output steps") = type::switch_(map.at("output steps")) (
                    type::case_<int>([](auto i) {return boost::lexical_cast<size_t>(i);}),
                    type::case_<float>([](auto d) {return boost::lexical_cast<size_t>(d);}),
                    type::default_([] {return 0;}));
                map.at("propagation steps") = type::switch_(map.at("propagation steps")) (
                    type::case_<int>([](auto i) {return boost::lexical_cast<size_t>(i);}),
                    type::case_<float>([](auto d) {return boost::lexical_cast<size_t>(d);}),
                    type::default_([] {return 0;}));
                map.at("simulation steps") = type::switch_(map.at("simulation steps")) (
                    type::case_<int>([](auto i) {return boost::lexical_cast<size_t>(i);}),
                    type::case_<float>([](auto d) {return boost::lexical_cast<size_t>(d);}),
                    type::default_([] {return 0;}));
                block.second = map;
            }
    }
    catch (const std::exception& e) {
        std::cerr << "Parser::Parser\t" << e.what()<< "\t(missing parameter?)\n";
        throw;
    }
}


template<typename T>
T Parser<T>::at(const std::string& name) const {
    try {
        for (const auto& block : data_)
            if (block.first == boost::to_upper_copy(name))
                return boost::any_cast<T>(block.second);
    }
    catch (const std::exception& e) {
        std::cerr << "Parser::at\t" << e.what() << "\t" << "while trying '" << name << "'\n";
        throw;
    }

    throw std::invalid_argument("parser could not find element '" + name + "'");
    return T();
}


template<typename T>
boost::any Parser<T>::cast_(const std::string& token) const {
    std::vector<std::string> tokens;
    boost::split(tokens, token, boost::is_any_of(" \t"));

    boost::any value;
    try {
        bool is_single_value = tokens.size() == 1;
        if (is_single_value) {
            if (isInteger_(token))
                value = boost::lexical_cast<int>(token);
            else if (isFloat_(token))
                value = boost::lexical_cast<float>(token);
            else if (isBool_(token))
                value = boost::lexical_cast<bool>(token);
            else
                value = token;
        }
        else {
            std::vector<float> vec;
            for (auto& token : tokens) {
                if (!token.empty()) {
                    boost::trim(token);
                    vec.push_back(boost::lexical_cast<float>(token));
                }
            }
            value = vec;
        }
    }
    catch (const std::exception& e) {
        std::cerr << "Parser::cast\t" << e.what() << "\t" << "while trying '" << token << "'\n";
        throw;
    }

    return value;
}


template<typename T>
bool Parser<T>::getTokens_(const std::shared_ptr<std::fstream>& file, std::vector<std::string>& tokens) {
    std::string line;
    std::getline(*file, line);
    boost::split(tokens, line, boost::is_any_of(":"));
    for (auto& token : tokens)
        boost::trim(token);

    return file->good();
}


template<typename T>
bool Parser<T>::isBool_ (const std::string& token) const {
    bool is_true = (token == "true" || token == "1");
    bool is_false = (token == "false" || token == "0");
    return (is_true || is_false);
}


template<typename T>
bool Parser<T>::isFloat_ (const std::string& token) const {
    return std::all_of( std::begin(token), std::end(token), [](char c) {return (std::isdigit(c) || c == '-' || c == '.' || c == 'e');} );
}


template<typename T>
bool Parser<T>::isInteger_ (const std::string& token) const {
    return std::all_of( std::begin(token), std::end(token), [](char c) {return ((std::isdigit(c) || c == '-') && c != '.');} );
}


template<typename T>
void Parser<T>::sanitiseMap_(T& map) const {
    std::string name = boost::any_cast<std::string>(map.at("name"));
    std::string prefix = "PATCH ";
    if (name.find("RING") != std::string::npos) {
        prefix = "RING ";
        map.emplace("number", 1);
    }

    map["name"] = boost::replace_all_copy(name, prefix, "");
}
