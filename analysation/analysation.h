#pragma once
#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <Eigen/Geometry>
#include "box/box.h"
#include "analysation/bin.hpp"
#include "interface/options.hpp"

namespace ac = boost::accumulators;


class Analysation {
    typedef ac::accumulator_set<double, ac::features<ac::tag::sum, ac::tag::mean, ac::tag::variance>> Accumulator;

    const Options options_;
    Box box_;
    size_t bin_number_;
    std::vector<Bin> bins_;
    std::map<size_t, float> bond_volume_means_;
    std::map<size_t, float> core_volume_means_;
    std::vector<size_t> cluster_histogram_;
    std::vector<size_t> cluster_counter_;
    size_t number_state_bins_;
    size_t number_cluster_bins_;
    std::map<std::string, size_t> state_counter_;
    std::map<std::string, float> state_volume_means_;

    std::vector<Eigen::Vector3f> centerCluster_(const Cluster& cluster) const;
    std::vector<float> findClusterVolume_(const Cluster& cluster);
    void findClusterSizes_();
    float findStateVolume_();
    size_t findLongestKey_(const std::map<std::string, size_t>& map);
    void findMinimumBox_(Eigen::Vector3f& box_minimum, Eigen::Vector3f& box_maximum, const std::vector<Eigen::Vector3f>& particles) const;
    void loadBins_(const std::shared_ptr<std::fstream>& file);
    void printProgress_(const size_t& bin) const;
    void readBlock_(const std::shared_ptr<std::fstream>& file, Bin& bin);
    Eigen::Vector3f readVector_(const std::string& line);
    std::string shortenState_(const std::string& state);
    void writeClusterSizes_(const std::shared_ptr<std::fstream>& file_cluster, const std::shared_ptr<std::fstream>& file_distribution);
    void writeClusterVTK_(const std::shared_ptr<std::fstream>& file);

    template<typename K, typename V> std::map<K, V> mean_(const std::map<K, std::vector<V>>& map);

public:
    explicit Analysation(const Options& options);

    void run();
};


template<typename K, typename V>
std::map<K, V> Analysation::mean_(const std::map<K, std::vector<V>>& map) {
    std::map<K, V> map_means;
    for (auto& element : map)
        map_means[element.first] = util::mean(element.second);

    return map_means;
}
