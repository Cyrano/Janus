#include "analysation.h"


Analysation::Analysation(const Options& options) :
    options_(options),
    box_(options),
    bin_number_(options.as<size_t>("analyse")),
    bins_(),
    bond_volume_means_(),
    core_volume_means_(),
    cluster_histogram_(),
    cluster_counter_(box_.particle_number_+1),
    number_state_bins_(0),
    number_cluster_bins_(100),
    state_counter_(),
    state_volume_means_()
{
    loadBins_(options.file("configuration"));
}


std::vector<Eigen::Vector3f> Analysation::centerCluster_(const Cluster& cluster) const {
    std::vector<Eigen::Vector3f> positions_centered(cluster.size());
    positions_centered[0] = box_.size_ / 2.0;

    size_t counter = 0;
    for (const auto& particle : cluster) {
        size_t neighbour = cluster.neighbours().at(particle);
        size_t neighbour_index = std::distance(cluster.begin(), std::find(cluster.begin(), cluster.end(), neighbour));
        Eigen::Vector3f distance_vector = box_.potential_->distance(box_.particles_[neighbour], box_.particles_[particle]);
        positions_centered[counter++] = positions_centered[neighbour_index] + distance_vector;
    }

    return positions_centered;
}


void Analysation::findClusterSizes_() {
    std::map<size_t, std::vector<float>> bond_volumes;
    std::map<size_t, std::vector<float>> core_volumes;
    std::map<std::string, std::vector<float>> state_volumes;

    size_t bin_counter = 0;
    for (const auto& bin : bins_) {
        printProgress_(++bin_counter);

        box_.loadParticles(bin.particles_);
        box_.updateCells();
        box_.updateClusters();

        for (const auto& cluster : box_.clusters_) {
            size_t cluster_identifier = cluster.size();
            cluster_histogram_.push_back(cluster_identifier);
            if (cluster_counter_[cluster_identifier] < number_cluster_bins_) {
                std::vector<float> volumes = findClusterVolume_(cluster);
                core_volumes[cluster_identifier].push_back(volumes[0]);
                bond_volumes[cluster_identifier].push_back(volumes[1]);
            }

            ++cluster_counter_[cluster_identifier];
        }

        std::string state_identifier = box_.clusters_.listShort();
        if (state_counter_[state_identifier] < number_state_bins_)
            state_volumes[state_identifier].push_back(findStateVolume_());

        ++state_counter_[state_identifier];
    }

    state_volume_means_ = mean_(state_volumes);
    bond_volume_means_ = mean_(bond_volumes);
    core_volume_means_ = mean_(core_volumes);
}


std::vector<float> Analysation::findClusterVolume_(const Cluster& cluster) {
    std::vector<Eigen::Vector3f> positions_centered = centerCluster_(cluster);
    Eigen::Vector3f box_minimum;
    Eigen::Vector3f box_maximum;
    findMinimumBox_(box_minimum, box_maximum, positions_centered);

    size_t sample_number = 10000 * cluster.size();
    size_t sample_number_bond = 0;
    size_t sample_number_core = 0;
    float distance_core = box_.particle_diameter_maximum_ / 2.0;
    float distance_bond = distance_core + box_.potential_->range() / 2.0;
    Eigen::Vector3f sample_position(Eigen::Vector3f::Zero());
    for (size_t sample = 0; sample < sample_number; ++sample) {
        for (size_t i = 0; i < box_.dimensions_; ++i)
            sample_position[i] = box_.random_.real(box_minimum[i], box_maximum[i]);

        size_t state_bond = 0;
        for (size_t i = 0; i < positions_centered.size(); ++i) {
            float distance = (sample_position - positions_centered[i]).norm();

            if (distance <= distance_core)
                state_bond = 1;
            else if (state_bond != 1 && distance <= distance_bond)
                state_bond = 2;
        }

        if (state_bond == 1)
            ++sample_number_core;
        else if (state_bond == 2)
            ++sample_number_bond;
    }

    float box_volume = 1.0;
    for (size_t i = 0; i < box_.dimensions_; ++i)
        box_volume *= box_maximum[i] - box_minimum[i];

    std::vector<float> volumes(2);
    volumes[0] = box_volume * static_cast<float>(sample_number_core) / sample_number;
    volumes[1] = box_volume * static_cast<float>(sample_number_bond) / sample_number;

    return volumes;
}


float Analysation::findStateVolume_() {
    std::vector<Eigen::Vector3f> positions_centered = box_.centeredPositions();
    Eigen::Vector3f box_minimum;
    Eigen::Vector3f box_maximum;
    findMinimumBox_(box_minimum, box_maximum, positions_centered);

    size_t sample_number = 10000 * box_.particle_number_;
    size_t sample_number_excluded = 0;
    float distance_minimum = box_.particle_diameter_maximum_ + box_.potential_->range();
    for (size_t sample = 0; sample < sample_number; ++sample) {
        Eigen::Vector3f sample_position(Eigen::Vector3f::Zero());
        for (size_t i = 0; i < box_.dimensions_; ++i)
            sample_position[i] = box_.random_.real(box_minimum[i], box_maximum[i]);

        for (size_t i = 0; i < box_.particle_number_; ++i) {
            float distance = (sample_position - positions_centered[i]).norm();

            if (distance <= distance_minimum) {
                ++sample_number_excluded;
                break;
            }
        }
    }
    float box_volume = 1.0;
    for (size_t i = 0; i < box_.dimensions_; ++i)
        box_volume *= box_maximum[i] - box_minimum[i];

    return box_volume * static_cast<float>(sample_number_excluded) / sample_number;
}


size_t Analysation::findLongestKey_(const std::map<std::string, size_t>& map) {
    size_t size_longest = 0;
    for (const auto& key : map) {
        size_t size_key = key.first.size();
        if (size_key > size_longest)
            size_longest = size_key;
    }

    return size_longest;
}


void Analysation::findMinimumBox_(Eigen::Vector3f& box_minimum, Eigen::Vector3f& box_maximum, const std::vector<Eigen::Vector3f>& particles) const {
    box_minimum = box_.size_;
    box_maximum = Eigen::Vector3f::Zero();
    for (const auto& particle : particles)
        for (size_t j = 0; j < box_.dimensions_; ++j) {
            box_minimum[j] = std::min(particle[j], box_minimum[j]);
            box_maximum[j] = std::max(particle[j], box_maximum[j]);
        }
    for (size_t j = 0; j < box_.dimensions_; ++j) {
        box_minimum[j] -= box_.particle_diameter_maximum_ + box_.potential_->range();
        box_maximum[j] += box_.particle_diameter_maximum_ + box_.potential_->range();
    }
}


void Analysation::loadBins_(const std::shared_ptr<std::fstream>& file) {
    // find begin of first block
    std::string line;
    std::getline(*file, line);
    size_t timeline_first = boost::lexical_cast<size_t>(line.erase(0, 4));

    // find begin of second block and number of characters per block
    size_t timeline_second = 0;
    size_t characters_per_block = 0;
    bool is_timeline = false;
    while (file.get()->good() && !is_timeline) {
        std::getline(*file, line);

        if (!line.empty() && line[0] == 't') {
            characters_per_block = static_cast<size_t>(file.get()->tellg()) - util::digitsInNumber(timeline_first) - line.size() - 1;
            timeline_second = boost::lexical_cast<size_t>(line.erase(0, 4));
            is_timeline = true;
        }
    }

    // find end of file
    file.get()->seekg(0, std::ios_base::end);
    size_t file_end = static_cast<size_t>(file.get()->tellg());

    // find positions of data blocks
    std::vector<size_t> file_positions;
    size_t file_position = 0;
    size_t timeline = timeline_first;
    // characters_per_block -= util::digitsInNumber(timeline_second);
    size_t timeline_delta = timeline_second - timeline_first;
    while (file_position < file_end) {
        file_positions.push_back(file_position);
        file_position += characters_per_block + util::digitsInNumber(timeline);
        timeline += timeline_delta;
    }

    size_t position_interval = std::ceil(file_positions.size() / bin_number_);
    bool is_interval_invalid = (position_interval < 1);
    if (is_interval_invalid)
        throw std::invalid_argument("number of bins too large");

    bins_.reserve(bin_number_);
    for (size_t position = 1; position < file_positions.size(); ++position) {
        if (position % position_interval == 0) {
            file.get()->seekg(file_positions[position]);
            bins_.emplace_back(Bin(box_.particle_number_));
            readBlock_(file, bins_.back());
        }
    }
}


void Analysation::printProgress_(const size_t& bin) const {
    if (options_.count("progress")) {
        bool is_clear_due = bin == bins_.size();
        if (is_clear_due)
            std::cout << "                                        \r" << std::flush;
        else {
            std::cout << "finished bin " << bin << " (" << std::fixed << std::showpoint
                      << std::setprecision(1) << static_cast<float>(bin) / bins_.size() * 100 << '%' << ")\r"
                      << std::flush;
        }
    }
}


void Analysation::readBlock_(const std::shared_ptr<std::fstream>& file, Bin& bin) {
    std::string line;
    std::getline(*file, line);
    bin.step_number_ = boost::lexical_cast<size_t>(line.erase(0, 4));
    std::getline(*file, line);

    for (size_t particle = 0; particle < box_.particle_number_; ++particle) {
        // read particle position
        std::getline(*file, line);
        Eigen::Vector3f position = readVector_(line);
        bin.particles_[particle].position(position);

        // read particle orientation
        std::getline(*file, line);
        if (line.find("PATCH") == std::string::npos)
            bin.particles_[particle].orientation(Eigen::Vector3f::Zero());
        else {
            Eigen::Vector3f orientation = readVector_(line) - position;
            bin.particles_[particle].orientation(orientation);

            // skip other patches
            size_t file_position = 0;
            while (line.find("PATCH") != std::string::npos) {
                file_position = file.get()->tellg();
                std::getline(*file, line);
            }
            file.get()->seekg(file_position);
        }
    }
}


Eigen::Vector3f Analysation::readVector_(const std::string& line) {
    Eigen::Vector3f vector;
    for (int dim = 0; dim < 3; ++dim) {
        std::string value = boost::trim_left_copy(line.substr(20 + box_.vmd_precision_ * dim, 20 + box_.vmd_precision_ * (dim+1)));
        vector[dim] = std::atof(value.c_str());
    }

    return vector / box_.vmd_scaling_;
}


void Analysation::run() {
    findClusterSizes_();
    writeClusterSizes_(options_.file("clusters"), options_.file("analyse"));

    if (options_.count("vtk"))
        writeClusterVTK_(options_.file("vtk"));
}


void Analysation::writeClusterSizes_(const std::shared_ptr<std::fstream>& file_cluster, const std::shared_ptr<std::fstream>& file_distribution) {
    file_cluster->setf(std::ios::fixed|std::ios::showpoint);
    *file_cluster << std::setprecision(6);
    *file_cluster << "# bin\t\tvalue\n";
    auto histogram = util::histogram(cluster_histogram_);
    for (const auto& element : histogram)
        *file_cluster << element.first << "\t" << element.second << "\n";

    std::stringstream stream;
    stream.setf(std::ios::fixed|std::ios::showpoint);
    size_t key_length_max = findLongestKey_(state_counter_);

    size_t largest_populated_cluster = 0;
    for (size_t i = 0; i < cluster_counter_.size(); ++i)
        if (cluster_counter_[i] != 0)
            largest_populated_cluster = i;

    size_t precision = util::digitsInNumber(bin_number_);
    stream << "cluster distribution:\n"
           << "\t" << std::left << std::setw(key_length_max) << "n" << "\t" << std::setw(8) << "f(abs)"
           << "\t" << std::setw(precision+2) << "f(rel)" << "\t" << std::setw(precision+2) << "f(rel)"
           << "\tV(ex)\t\tV(core)\t\tV(bond)\n";
    size_t cluster_number_total = std::accumulate(cluster_counter_.begin(), cluster_counter_.end(), 0);
    for (size_t i = 1; i <= largest_populated_cluster; ++i)
        stream << "\t" << std::left << std::setw(key_length_max) << i
               << "\t" << std::right << std::setw(8) << cluster_counter_[i]
               << "\t" << std::setw(precision+2) << std::setprecision(precision) << static_cast<float>(cluster_counter_[i]) / cluster_number_total
               << "\t" << std::setw(precision+2) << std::setprecision(precision) << static_cast<float>(cluster_counter_[i]) * i / bin_number_ / box_.particle_number_
               << "\t" << std::setw(8) << std::setprecision(4) << core_volume_means_[i] + bond_volume_means_[i]
               << "\t" << std::setw(8) << core_volume_means_[i]
               << "\t" << std::setw(8) << bond_volume_means_[i] << "\n";

    stream << "state distribution:\n"
           << "\t" << std::left << std::setw(key_length_max) << "state" << "\t" << std::setw(8) << "f(abs)"
           << "\t" << std::setw(precision+2) << "f(rel)" << "\tV(ex)\n";
    for (const auto& state : state_counter_)
        stream << "\t" << std::left << std::setw(key_length_max) << state.first
               << "\t" << std::right << std::setw(8) << state.second
               << "\t" << std::setw(precision+2) << std::setprecision(precision-1) << static_cast<float>(state.second) / bins_.size()
               << "\t" << std::setw(8) << std::setprecision(4) << state_volume_means_[state.first] << "\n";

    *file_distribution << stream.str();
}


void Analysation::writeClusterVTK_(const std::shared_ptr<std::fstream>& file) {
    for (size_t bin = 0; bin < bins_.size(); ++bin) {
        box_.loadParticles(bins_[bin].particles_);
        box_.updateCells();
        box_.updateClusters();

        *file << "state " << bin << ":\t" << box_.clusters_.list()<< "\n";
        for (const auto& cluster : box_.clusters_) {
            *file << cluster.size() << "\t" << box_.potential_->range() << "\n";

            for (const auto& id : cluster.particles()) {
                auto displacement = box_.potential_->distance(box_.particles_[id], box_.particles_[cluster.particles()[0]]);
                *file << displacement[0] << "\t" << displacement[1] << "\t" << displacement[2] << "\t" << box_.particles_[id].radius() << "\n";
            }
            *file << "\n";
        }

        if (bin < bins_.size()-1)
            *file << "\n";
    }
}
