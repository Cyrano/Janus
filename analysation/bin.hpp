#pragma once
#include <vector>
#include "particle/particle.hpp"


class Analysation;

class Bin {
    friend Analysation;

    explicit Bin(const size_t& particle_number) :
        particles_(particle_number),
        step_number_(0)
    {};

    std::vector<ParticleBase> particles_;
    size_t step_number_;
};
