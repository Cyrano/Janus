#pragma once
#include <exception>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <Eigen/Geometry>
#include "shared/constants.hpp"
#include "shared/map.hpp"
#include "shared/utilities.hpp"
#include "particle/patch.hpp"
#include "particle/ring.hpp"


class ParticleBase {
protected:
    Eigen::Vector3f orientation_;
    Eigen::Vector3f position_;

public:
    ParticleBase() : orientation_(Eigen::Vector3f::Zero()), position_(Eigen::Vector3f::Zero()) {};
    virtual ~ParticleBase() {};

    virtual void orientation(Eigen::Vector3f orientation) {orientation_ = orientation;}
    void position(const Eigen::Vector3f& position) {position_ = position;}

    const auto& orientation() const {return orientation_;}
    const auto& position() const {return position_;}
};


class Particle : public ParticleBase {
    const float diameter_;
    const size_t dimensions_;
    const std::string name_;
    const float radius_;
    size_t cell_;
    int cluster_;
    size_t id_;
    std::vector<Patch> patches_;
    std::vector<Ring> rings_;

    void setGeometry_();
    std::vector<Eigen::Vector3f> setPointsOnSphere_(const size_t& number) const;

public:
    using ParticleBase::orientation;

    Particle() = delete;
    Particle(const std::string& dummy);
    Particle(const Particle& other);
    explicit Particle(const Map<>& properties);

    void cell(const size_t& cell) {cell_ = cell;}
    void cluster(const int& cluster) {cluster_ = cluster;}
    void id(const size_t& id) {id_ = id;}
    void orientation(Eigen::Vector3f orientation);
    void orientation(const Particle& other);
    void remainBoxed(const Eigen::Vector3f& box);
    void rotate(const Eigen::AngleAxisf& rotation);
    void rotate(const Eigen::Quaternionf& rotation);
    void translate(const Eigen::Vector3f& translation) {position_ += translation;}

    const auto& cell() const {return cell_;}
    const auto& cluster() const {return cluster_;}
    const auto& diameter() const {return diameter_;}
    const auto& id() const {return id_;}
    const auto& name() const {return name_;}
    const auto& patches() const {return patches_;}
    const auto& patch(const size_t& index) const {return patches_[index];}
    const auto& radius() const {return radius_;}
    const auto& rings() const {return rings_;}
    float space() const;
};


inline Particle::Particle(const std::string& dummy) :
    ParticleBase(),
    diameter_(0.0),
    dimensions_(0),
    name_(dummy),
    radius_(0.0),
    cell_(0),
    cluster_(-1),
    id_(0),
    patches_(),
    rings_()
{
}


inline Particle::Particle(const Map<>& properties) try :
    ParticleBase(),
    diameter_(properties.as<float>("diameter")),
    dimensions_(properties.as<size_t>("dimensions")),
    name_(boost::replace_all_copy(properties.as<std::string>("name"), "PARTICLE ", "")),
    radius_(diameter_ / 2.0),
    cell_(0),
    cluster_(-1),
    id_(0),
    patches_(util::make<Patch>(properties, "patch")),
    rings_(util::make<Ring>(properties, "ring"))
{
    if (patches_.size() > 0)
        setGeometry_();
}
catch (const std::exception& e) {
    std::cerr << "Particle::Particle\t'" << e.what() << "'\n";
    throw;
}


inline Particle::Particle(const Particle& other) try :
    ParticleBase(other),
    diameter_(other.diameter_),
    dimensions_(other.dimensions_),
    name_(other.name_),
    radius_(other.radius_),
    cell_(other.cell_),
    cluster_(other.cluster_),
    id_(other.id_),
    patches_(other.patches_),
    rings_(other.rings_)
{
}
catch (const std::exception& e) {
    std::cerr << "Particle::Particle(const Particle&)\t" << e.what() << "'\n";
    throw;
}


inline void Particle::orientation(Eigen::Vector3f orientation) {
    orientation.normalize();
    Eigen::Quaternionf rotation = Eigen::Quaternionf::FromTwoVectors(orientation_, orientation);
    orientation_ = orientation;

    for (auto& patch : patches_)
        patch.orientation(rotation * patch.orientation());
}


inline void Particle::orientation(const Particle& other) {
    orientation_ = other.orientation_;

    for (size_t i = 0; i < patches_.size(); ++i)
        patches_[i].orientation(other.patches_[i].orientation());
}


inline void Particle::rotate(const Eigen::AngleAxisf& rotation) {
    orientation_ = (rotation * orientation_).normalized();

    for (auto& patch : patches_)
        patch.orientation((rotation * patch.orientation()).normalized());
}


inline void Particle::rotate(const Eigen::Quaternionf& rotation) {
    orientation_ = rotation * orientation_;

    for (auto& patch : patches_)
        patch.orientation(rotation * patch.orientation());
}


inline void Particle::remainBoxed(const Eigen::Vector3f& box) {
    for (size_t i = 0; i < dimensions_; i++) {
        if (position_[i] >= box[i])
            position_[i] -= box[i];
        else if (position_[i] < 0)
            position_[i] += box[i];
    }
}


inline float Particle::space() const {
    switch (dimensions_) {
        case 1: return diameter_;
        case 2: return diameter_ * diameter_ * PI / 4.0;
        case 3: return diameter_ * diameter_ * diameter_ * PI / 6.0;
    }

    return 0.0;
}


inline void Particle::setGeometry_() {
    orientation_ = Eigen::Vector3f::UnitX();
    patches_[0].orientation(orientation_);

    switch (patches_.size()) {
        case 1:
            break;
        case 2: {
            patches_[1].orientation(-orientation_);

            break;
        }
        case 3: {
            Eigen::AngleAxisf rotation_z(PI * 2.0 / 3.0,  Eigen::Vector3f::UnitZ());

            for (size_t i = 1; i < patches_.size(); ++i)
                patches_[i].orientation(rotation_z * patches_[i-1].orientation());

            break;
        }
        case 4: {
            Eigen::AngleAxisf rotation_y(PI * 0.60833333333, Eigen::Vector3f::UnitY());
            Eigen::AngleAxisf rotation_z(PI * 2.0 / 3.0, Eigen::Vector3f::UnitZ());

            patches_[0].orientation(Eigen::Vector3f::UnitZ());
            patches_[1].orientation(rotation_y * patches_[0].orientation());

            for (size_t i = 2; i < patches_.size(); ++i)
                patches_[i].orientation(rotation_z * patches_[i-1].orientation());

            break;
        }
        case 5: {
            Eigen::AngleAxisf rotation_z(PI * 2.0 / 3.0, Eigen::Vector3f::UnitZ());

            patches_[0].orientation(Eigen::Vector3f::UnitZ());
            patches_[1].orientation(-Eigen::Vector3f::UnitZ());
            patches_[2].orientation(Eigen::Vector3f::UnitX());

            for (size_t i = 3; i < patches_.size(); ++i)
                patches_[i].orientation(rotation_z * patches_[i-1].orientation());

            break;
        }
        case 6: {
            Eigen::AngleAxisf rotation_z(PI / 2.0, Eigen::Vector3f::UnitZ());

            patches_[0].orientation(Eigen::Vector3f::UnitZ());
            patches_[1].orientation(-Eigen::Vector3f::UnitZ());
            patches_[2].orientation(Eigen::Vector3f::UnitX());

            for (size_t i = 3; i < patches_.size(); ++i)
                patches_[i].orientation(rotation_z * patches_[i-1].orientation());

            break;
        }
        case 7: {
            Eigen::AngleAxisf rotation_z(PI * 2.0 / 5.0, Eigen::Vector3f::UnitZ());

            patches_[0].orientation(Eigen::Vector3f::UnitZ());
            patches_[1].orientation(-Eigen::Vector3f::UnitZ());
            patches_[2].orientation(Eigen::Vector3f::UnitX());

            for (size_t i = 3; i < patches_.size(); ++i)
                patches_[i].orientation(rotation_z * patches_[i-1].orientation());

            break;
        }
        case 8: {
            Eigen::Vector3f unit111(1.0, 0.0, 1.0);
            Eigen::Vector3f unit111m = Eigen::Vector3f::Constant(-1.0);
            Eigen::AngleAxisf rotation_z(PI / 2.0, Eigen::Vector3f::UnitZ());

            patches_[0].orientation(unit111.normalized());
            patches_[4].orientation(unit111m.normalized());

            for (size_t i = 1; i < patches_.size(); ++i) {
                if (i == 4)
                    continue;

                patches_[i].orientation(rotation_z * patches_[i-1].orientation());
            }

            break;
        }
        default: {
            std::vector<Eigen::Vector3f> orientations = setPointsOnSphere_(patches_.size());

            for (size_t i = 0; i < patches_.size(); ++i)
                patches_[i].orientation(orientations[i]);

            break;
        }
    }

    // if (boost::to_lower_copy(name_) == "water") {
    //     if (patches_.size() != 2)
    //         throw std::invalid_argument("water particles have to have two patches");

    //     float angle = 0.911934;
    //     Eigen::AngleAxisf rotation_a(angle, Eigen::Vector3f::UnitZ());
    //     Eigen::AngleAxisf rotation_b(-angle, Eigen::Vector3f::UnitZ());
    //     patches_[0].orientation(rotation_a * Eigen::Vector3f::UnitX());
    //     patches_[1].orientation(rotation_b * Eigen::Vector3f::UnitX());
    // }
}


inline std::vector<Eigen::Vector3f> Particle::setPointsOnSphere_(const size_t& number) const {
    std::vector<Eigen::Vector3f> orientations(number);
    orientations.front() = Eigen::Vector3f::UnitZ();
    orientations.back() = -Eigen::Vector3f::UnitZ();

    float phi = 0.0;
    float theta = 0.0;
    for (size_t i = 1; i < number-1; ++i) {
        float angle = static_cast<float>(i * 2) / (number - 1) - 1.0;
        theta = std::acos(angle);
        phi += 3.6 / (std::sqrt(number) * std::sqrt(-angle * angle + 1.0));

        while (phi >= PI * 2.0)
            phi -= PI * 2.0;
        while (phi < 0.0)
            phi += PI * 2.0;

        Eigen::AngleAxisf rotation_y(theta, Eigen::Vector3f::UnitY());
        Eigen::AngleAxisf rotation_z(phi, Eigen::Vector3f::UnitZ());

        orientations[i] = (rotation_z * rotation_y * -Eigen::Vector3f::UnitZ()).normalized();
    }

    return orientations;
}
