#pragma once
#include <cmath>
#include <exception>
#include <iostream>
#include <string>
#include <Eigen/Geometry>
#include "shared/map.hpp"


class Patch {
    const float charge_;
    const std::string name_;
    Eigen::Vector3f orientation_;
    const float size_;
    const float sizeCos_;

public:
    Patch() = delete;
    explicit Patch(const Map<>& properties);

    void orientation(const Eigen::Vector3f& orientation) {orientation_ = orientation;}

    const auto& charge() const {return charge_;}
    const auto& name() const {return name_;}
    const auto& orientation() const {return orientation_;}
    const auto& size() const {return size_;}
    const auto& sizeCos() const {return sizeCos_;}
};


inline Patch::Patch(const Map<>& properties) try :
    charge_(properties.as<float>("charge")),
    name_(properties.as<std::string>("name")),
    orientation_(Eigen::Vector3f::Zero()),
    size_(properties.as<float>("size")),
    sizeCos_(std::cos(size_))
{
}
catch (const std::exception& e) {
    std::cerr << "Patch::Patch\t" << e.what() << "'\n";
    throw;
}
