#pragma once
#include <vector>
#include <Eigen/Geometry>
#include "particle/clusters.hpp"
#include "particle/particle.hpp"
#include "shared/vtk.h"

class Powercrust {

public:
    float calc(const std::vector<Particle>& particles, const Cluster& cluster, const float& range) const;
    float calc(const std::vector<Particle>& particles, const Cluster& cluster, const float& range, const std::string& file_stem) const;
};


inline float Powercrust::calc(const std::vector<Particle>& particles, const Cluster& cluster, const float& range) const {
    return calc(particles, cluster, range, std::string());
}


inline float Powercrust::calc(const std::vector<Particle>& particles, const Cluster& cluster, const float& range, const std::string& file_stem) const {
    auto points = vtkSmartPointer<vtkPoints>::New();
    for (const auto& id : cluster.particles()) {
        for (size_t i = 0; i < orientations_.size(); ++i) {
            auto point = particles[id].position() + orientations_[i] * (particles[id].radius() + range);
            points->InsertNextPoint(point[0], point[1], point[2]);
        }
    }

    auto polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->SetPoints(points);

    auto surface_reconstructor = vtkSmartPointer<vtkPowerCrustSurfaceReconstruction>::New();
    surface_reconstructor->SetInputData(polydata);
    surface_reconstructor->Update();

    auto triangle_filter = vtkSmartPointer<vtkTriangleFilter>::New();
    triangle_filter->SetInputData(surface_reconstructor->GetOutput());
    triangle_filter->Update();

    if (!file_stem.empty()) {
        auto polydata_writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
        polydata_writer->SetInputData(triangle_filter->GetOutput());
        polydata_writer->SetFileName((file_stem + ".cluster" + std::to_string(cluster.id()) + ".vtp").c_str());
        polydata_writer->Write();
    }

    auto structure_properties = vtkSmartPointer<vtkMassProperties>::New();
    structure_properties->SetInputData(triangle_filter->GetOutput());
    structure_properties->Update();

    return structure_properties->GetVolume();
}
