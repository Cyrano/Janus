#pragma once
#include <cmath>
#include <exception>
#include <iostream>
#include <string>
#include "shared/map.hpp"


class Ring {
    const float charge_;
    const std::string name_;
    const float size_;
    const float orientation_;
    const float max_;
    const float min_;

public:
    Ring() = delete;
    explicit Ring(const Map<>& properties);

    void orientation(const float& orientation);

    const auto& charge() const {return charge_;}
    const auto& max() const {return max_;}
    const auto& min() const {return min_;}
    const auto& name() const {return name_;}
    const auto& orientation() const {return orientation_;}
    const auto& size() const {return size_;}
};


inline Ring::Ring(const Map<>& properties) try :
    charge_(properties.as<float>("charge")),
    name_(properties.as<std::string>("name")),
    size_(properties.as<float>("size")),
    orientation_(properties.as<float>("orientation")),
    max_(std::cos(orientation_ - size_)),
    min_(std::cos(orientation_ + size_))
{
}
catch (const std::exception& e) {
    std::cerr << "Ring::Ring\t" << e.what() << "'\n";
    throw;
}
