find_package(PkgConfig)
pkg_check_modules(PC_EIGEN QUIET eigen3)
set(EIGEN_DEFINITIONS ${PC_EIGEN_CFLAGS_OTHER})

find_path(EIGEN_INCLUDE_DIR Eigen/Core HINTS ${PC_EIGEN_INCLUDEDIR} ${PC_EIGEN_INCLUDE_DIRS} /usr/include PATH_SUFFIXES eigen3)
set(EIGEN_INCLUDE_DIRS ${EIGEN_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Eigen DEFAULT_MGS EIGEN_INCLUDE_DIR)
mark_as_advanced(EIGEN_INCLUDE_DIR)
