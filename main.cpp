#include <iostream>
#include <memory>
#include "analysation/analysation.h"
#include "interface/options.hpp"
#include "shared/clock.hpp"
#include "simulation/simulation.h"

/* TODO
- general: error handling
- general: implement iterators
- box: state class
- simulation: move stuff to box
- parser: avoid raw() somehow?

- properties, state, save
- inherit analysation from box
- parallelisation

- EKH potential
- landscape
*/


int main(int argc, char** argv) {
    Clock clock;
    Options options(argc, argv);

    if (options.count("analyse")) {
        Analysation analysation(options);

        clock.reset();
        analysation.run();
        std::cout << "analysation:\t" << clock.elapsed() << "\n";

        return 0;
    }

    std::unique_ptr<Simulation> simulation;
    if (options.isMonteCarlo()) {
        if (options.count("maximum"))
            simulation = std::make_unique<MonteCarloMaximum>(options);
        else if (options.count("distribution")) {
            if (options.distribution() == "clusters")
                simulation = std::make_unique<MonteCarloClusters>(options);
            else if (options.distribution() == "states")
                simulation = std::make_unique<MonteCarloStates>(options);
        }
        else
            simulation = std::make_unique<MonteCarlo>(options);
    }
    else if (options.isWangLandau())
            simulation = std::make_unique<WangLandau>(options);

    clock.reset();
    simulation->run();
    std::cout << "simulation:\t" << clock.elapsed() << "\n";

    return 0;
}
