#pragma once
#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include "box/box.h"
#include "interface/options.hpp"


class Simulation {
protected:
    const Options options_;
    Box box_;
    size_t step_;

    void attemptRotation_();
    virtual void attemptStep_() = 0;
    void attemptTranslation_();
    void attemptTranslationClusterupdate_();
    void checkAcceptance_();

public:
    explicit Simulation(const Options& options);
    virtual ~Simulation() {};

    virtual void run() = 0;
};


class MonteCarlo : public Simulation {
protected:
    const size_t steps_output_;
    const size_t steps_propagation_;
    const size_t steps_simulation_;
    const size_t output_interval_;

    virtual void attemptStep_();
    void printProgress_(const size_t& step_number) const;
    void writeOutput_();

public:
    explicit MonteCarlo(const Options& options);

    void run();
};


class MonteCarloMaximum : public MonteCarlo {
    virtual void attemptStep_();

public:
    explicit MonteCarloMaximum(const Options& options) : MonteCarlo(options) {};
};


class MonteCarloClusters : public MonteCarlo {
    std::vector<size_t> cluster_counter;

    void checkClusters_();
    void printClusters_();

public:
    explicit MonteCarloClusters(const Options& options) : MonteCarlo(options), cluster_counter(box_.particleNumber()+1) {};

    void run();
};


class MonteCarloStates : public MonteCarlo {
    std::vector<size_t> state_counter;

    void checkStates_();
    void printStates_();

public:
    explicit MonteCarloStates(const Options& options) : MonteCarlo(options), state_counter(box_.particleNumber()+1) {};

    void run();
};


class WangLandau : public Simulation {
    const float flatness_target_;
    const size_t number_stages_;
    const size_t number_substages_;
    double increment_;
    size_t interval_;
    size_t interval_base_;
    size_t stage_;
    size_t substage_;

    void attemptStep_();
    bool checkHistograms_();
    void checkStage_();
    void evaluate_();
    void populateHistograms_();
    void printProgress_() const;
    void resetHistograms_() {box_.resetHistograms();}
    void updateAccumulators_() {box_.updateWangLandau(increment_);}

public:
    explicit WangLandau(const Options& options);

    void run();
};
