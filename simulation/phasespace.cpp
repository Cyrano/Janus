#include "phasespace.h"

Phasespace::Phasespace (const Options& options) {
    mode_(options_.mode()),
    steps_distance_(options.at("integration").as<size_t>("steps distance")),
    steps_phi_orientation_(options.at("integration").as<size_t>("steps phi orientation")),
    steps_phi_position_(1), // ignored for symmetric particles, as integral is always 2π
    steps_theta_orientation_(options.at("integration").as<size_t>("steps theta orientation")),
    steps_theta_position_(options.at("integration").as<size_t>("steps theta position")),
    distance_minimum_(particle[0].radius() + particle[1].radius()),
    delta_distance_(options.at("potential").as<float>("range") / stepsDistance),
    delta_phi_orientation_(2.0 * PI / steps_phi_orientation_),
    delta_phi_position_(2.0 * PI / steps_phi_position_),
    delta_theta_orientation_(PI / steps_theta_orientation_),
    delta_theta_position_(PI / steps_theta_position_),
    delta_distance_increment_(0.5),
    delta_phi_orientation_increment_(0.5),
    delta_phi_position_increment_(0.0),
    delta_theta_orientation_increment_(0.5),
    delta_theta_position_increment_(0.5),
    system_orientation_z_(Eigen::Vector3f::UnitZ()),
    system_orientation_x_(Eigen::Vector3f::UnitY()),
    system_position_z_(Eigen::Vector3f::UnitZ()),
    system_position_x_(Eigen::Vector3f::UnitY()),
    linebreak_point_("\n"),
    linebreak_block_("\n"),
    linebreak_distance_(""),
    volume_analytical_(0),
    volume_free_(0),
    volume_numerical_(0)
}


void Phasespace::probability_() const {
    // calculate factor for patch interaction
    float patchInteraction = patchInteraction_(particle_[0].patch[0], particle_[1].patch[0]);

    // one-dimensional case
    if (parameters_.boxDimensions() == 1) {
        // calculate numerical cluster volume
        phasespace.volumeNumerical = ac::sum(phasespace.acVolume) * phasespace.deltaDistance / 2.0;

        // calculate analytical cluster volume
        float volumeBox = parameters_.boxSize()[0];
        float volumeInteraction = 2.0 * parameters_.forceInteractionLength(); // phasespace.distanceMinimum *
        phasespace.volumeAnalytical = (std::exp(-patchInteraction / parameters_.boxTemperature()) + 3.0) / 4.0 * volumeInteraction;
        phasespace.volumeFree = volumeBox - 2.0 * (phasespace.distanceMinimum + parameters_.forceInteractionLength());
    }

    // higher-dimensional case
    else {
    // calculate numerical cluster volume
    float volumeSphere = 0.0;
    float factorNumerical = 0.0;
    if (parameters_.boxDimensions() == 2) {
      volumeSphere = PI;
      factorNumerical = phasespace.deltaDistance * phasespace.deltaThetaPosition * phasespace.deltaPhiOrientation;
    }
    if (parameters_.boxDimensions() == 3) {
      volumeSphere = 4.0 / 3.0 * PI;
      factorNumerical = phasespace.deltaDistance * phasespace.deltaThetaPosition * phasespace.deltaThetaOrientation * phasespace.deltaPhiOrientation;
    }
    phasespace.volumeNumerical = ac::sum(phasespace.acVolume) * factorNumerical / 2.0;

    // calculate analytical cluster volume
    float volumeBox = 1.0;
    for (int i = 0; i < parameters_.boxDimensions(); i++)
      volumeBox *= parameters_.boxSize()[i];
    float volumeParticle = volumeSphere * std::pow(phasespace.distanceMinimum, parameters_.boxDimensions());
    float volumeInteraction = volumeSphere * std::pow(phasespace.distanceMinimum + parameters_.forceInteractionLength(), parameters_.boxDimensions()) - volumeParticle;
    float patchFraction = 1.0;
    for (unsigned int i = 0; i < particle_.size(); i++)
      for (int j = 0; j < particle_[i].patchNumber(); j++)
        patchFraction *= (1.0 - std::cos(particle_[i].patch[j].size())) / 2.0;
    phasespace.volumeAnalytical = ((std::expm1(-patchInteraction / parameters_.boxTemperature())) * patchFraction + 1.0) * volumeInteraction; // expm1 = exp()-1
    phasespace.volumeFree = volumeBox - volumeSphere * std::pow(phasespace.distanceMinimum + parameters_.forceInteractionLength(), parameters_.boxDimensions());
    }

    // print cluster probabilities
    if (parameters_.forceName() != "EKH" || parameters_.fitDone()) {
    // print partition function and free energy obtained by integration
    shared::message("janus.evaluate", "Z(total) = " + boost::str(boost::format("%.4f") % (phasespace.volumeNumerical + phasespace.volumeFree))
                                                  + ", Z(clustered) = " + boost::str(boost::format("%.4f") % phasespace.volumeNumerical)
                                                  + ", F(clustered) = " + boost::str(boost::format("%.4f") % (-parameters_.boxTemperature() * std::log(phasespace.volumeNumerical)))
                                                  + ", Z(unclustered) = " + boost::str(boost::format("%.4f") % phasespace.volumeFree)
                                                  + ", F(unclustered) = " + boost::str(boost::format("%.4f") % (-parameters_.boxTemperature() * std::log(phasespace.volumeFree)))
                                                  + ", ΔF = " + boost::str(boost::format("%.4f") % (-parameters_.boxTemperature() * std::log(phasespace.volumeNumerical/phasespace.volumeFree))));

    // print numerical cluster probability
    shared::message("janus.evaluate", "cluster probability (numerical) is  " + boost::str(boost::format("%.4f") % (phasespace.volumeNumerical / (phasespace.volumeNumerical + phasespace.volumeFree))));

    // print analytical cluster probability for KF potential
    if (parameters_.forceName() == "KF" && parameters_.patchTypeNumber()(0) == 1)
      shared::message("janus.evaluate", "cluster probability (analytical) is  " + boost::str(boost::format("%.4f") % (phasespace.volumeAnalytical / (phasespace.volumeAnalytical + phasespace.volumeFree))));
    }
}


void Phasespace::analyse_(std::ofstream& filePotentialMap, std::ofstream& fileCoordinates) {
  // initialize two particles
  particle_[0].initialize(parameters_.boxSize(), phasespace.systemPositionZ);
  particle_[1].initialize(parameters_.boxSize(), phasespace.systemPositionZ);
  particle_[0].move(parameters_.boxSize() / 2.0);

  // loop from minimum distance to maximum distance
  int stepCounter = 0;
  for (int i = 0; i < phasespace.stepsDistance; i++) {
    float particleDistance = phasespace.distanceMinimum + phasespace.deltaDistance * (i + phasespace.deltaDistanceIncrement);
    mapAngles_(0, i) = particleDistance;

    // loop over particle position in theta dimension
    for (int j = 0; j < phasespace.stepsThetaPosition; j++) {
      float thetaPosition = phasespace.deltaThetaPosition * (j + phasespace.deltaThetaPositionIncrement);
      mapAngles_(1, j) = thetaPosition;

      // loop over particle position in phi dimension
      for (int k = 0; k < phasespace.stepsPhiPosition; k++) {
        float phiPosition = phasespace.deltaPhiPosition * (k + phasespace.deltaPhiPositionIncrement);
        mapAngles_(2, k) = phiPosition;

        // set position of second particle
        Eigen::Quaterniond quaternionPosition = Eigen::AngleAxisd(phiPosition, phasespace.systemPositionZ) * Eigen::AngleAxisd(thetaPosition, phasespace.systemPositionX);
        Eigen::Vector3f coordinates = quaternionPosition * phasespace.systemPositionZ;
        particle_[1].move(coordinates * particleDistance + parameters_.boxSize() / 2.0);

        // loop over particle orientation in theta dimension
        for (int l = 0; l < phasespace.stepsThetaOrientation; l++) {
          float thetaOrientation = phasespace.deltaThetaOrientation * (l + phasespace.deltaThetaOrientationIncrement);
          mapAngles_(3, l) = thetaOrientation;

          // loop over particle orientation in phi dimension
          for (int m = 0; m < phasespace.stepsPhiOrientation; m++) {
            float phiOrientation = phasespace.deltaPhiOrientation * (m + phasespace.deltaPhiOrientationIncrement);
            mapAngles_(4, m) = phiOrientation;

            // change particle orientation for perpendicular mode
            if (phasespace.stepsThetaOrientation == 1 && phasespace.stepsPhiOrientation == 1)
              thetaOrientation = thetaPosition;

            //  set orientation of second particle
            Eigen::Quaterniond quaternionOrientation = Eigen::AngleAxisd(phiOrientation, phasespace.systemOrientationZ) * Eigen::AngleAxisd(thetaOrientation, phasespace.systemOrientationX);
            Eigen::Vector3f orientation = quaternionOrientation * phasespace.systemOrientationZ;
            particle_[1].rotate(orientation);

            // calculate potential
            float potential = potential_(particle_);
            float microstate = std::exp(-potential / parameters_.boxTemperature());
            mapEnergies_[i][j][l][m] = potential;

            // save microstate according to dimensionality of the system
            if (parameters_.boxDimensions() == 1)
              phasespace.acVolume(microstate);
            if (parameters_.boxDimensions() == 2)
              phasespace.acVolume(particleDistance * std::sin(thetaPosition) * microstate);
            else
              phasespace.acVolume(particleDistance * particleDistance * std::sin(thetaPosition) * std::sin(thetaOrientation) * microstate);

            // write coordinate file
            if (parameters_.writeOutput() > 0)
              writePositions_(fileCoordinates, particle_, stepCounter);

            // write potential map file
            filePotentialMap << std::setw(9) << particleDistance << "\t"
                             << std::setw(4) << stepCounter - phasespace.stepsTotal() * i << "\t"
                             << std::setw(9) << thetaPosition << "\t" << std::setw(9) << phiPosition << "\t"
                             << std::setw(9) << thetaOrientation << "\t" << std::setw(9) << phiOrientation << "\t"
                             << std::setw(15) << std::setprecision(12) << potential << std::setprecision(5) << "\n";

            stepCounter++;
          }

          filePotentialMap << phasespace.pointLinebreak;
        }
      }

      filePotentialMap << phasespace.blockLinebreak;
    }

    filePotentialMap << phasespace.distanceLinebreak;
  }
}


size_t Phasespace::stepsMax_() const {
  return std::max({steps_theta_orientation_, steps_theta_position_, steps_phi_orientation_, steps_phi_position_});
}


size_t Phasespace::stepsTotal_() const {
  return steps_theta_orientation_ * steps_theta_position_ * steps_phi_orientation_ * steps_phi_position_;
}

*options_.file("trajectory")
void Phasespace::evaluate() const {
    // check evaluation mode
    if (evaluationMode != "map" && evaluationMode != "fit" && evaluationMode != "per" && evaluationMode != "par")
        throw std::invalic_argument("invalid evaluation mode '" + evaluationMode + "'");
    if (evaluationMode != "par" && parameters_.boxDimensions() != 3)
        throw std::invalic_argument("invalid number of dimensions for phasespace evaluation");

    // open output files
    std::ofstream filePotentialMap;
    std::ofstream fileCoordinates;
    if (evaluationMode != "fit") {
        filePotentialMap.open(filesOutput[1]);
        shared::setFormat(filePotentialMap, parameters_.outputPrecision());
        filePotentialMap << "# r\t\tstep\ttheta 1\t\tphi 1\t\ttheta 2\t\tphi 2\t\tu\n";

        if (parameters_.writeOutput() > 0)
          fileCoordinates.open(fileBasename + "_map.gro");
    }

    // construct phasespace for evaluation
    Phasespace phasespace(parameters_, particle_);

    // resize maps for angles and energies
    int mapSize = std::max(phasespace.stepsMax(), parameters_.evaluationStepsDistance());
    mapAngles_.resize(5, mapSize);
    mapEnergies_ = QuatrupelMatrix(parameters_.evaluationStepsDistance(),
                                 TripelMatrix(phasespace.stepsMax(),
                                              floatMatrix(phasespace.stepsMax(),
                                                           std::vector<float>(phasespace.stepsMax()))));

    // generate fit parameters
    if (evaluationMode == "fit" && !parameters_.fitDone()) {
        shared::message("janus.evaluate", "generating data for fit");

        // do analysation of phase space
        analyse_(phasespace, filePotentialMap, fileCoordinates);

        // fit electrostatic potential
        shared::message("janus.evaluate", "generating fit");
        parameters_.fit(fileBasename, mapAngles_, mapEnergies_);
    }

    // generate energy landscape
    if (evaluationMode == "map") {
        shared::message("janus.evaluate", "generating energy landscape");

        // change values of phasespace to please Granick
        phasespace.deltaThetaOrientation = 2.0 * PI / phasespace.stepsThetaOrientation;
        phasespace.deltaThetaOrientationIncrement = 0.0;
        phasespace.deltaThetaPositionIncrement = 0.0;
        phasespace.deltaPhiOrientationIncrement = 0.0;
        phasespace.systemOrientationZ = Eigen::Vector3f::UnitY();
        phasespace.systemOrientationX = Eigen::Vector3f::UnitX();

        // do analysation of phase space
        analyse_(phasespace, filePotentialMap, fileCoordinates);

        // calculate cluster probability
        probability_(phasespace);
    }

    // evaluate partition function
    if (evaluationMode == "par") {
        shared::message("janus.evaluate", "evaluating partition function");

        // one-dimensional case
        if (parameters_.boxDimensions() == 1) {
            // change values of phasespace
            phasespace.stepsThetaOrientation = 2;
            phasespace.stepsThetaPosition = 2;
            phasespace.stepsPhiOrientation = 1;
            phasespace.deltaThetaOrientation = PI;
            phasespace.deltaThetaPosition = PI;
            phasespace.deltaPhiOrientation = PI;
            phasespace.deltaThetaOrientationIncrement = 1.0;
            phasespace.deltaThetaPositionIncrement = 0.0;
            phasespace.systemOrientationZ = Eigen::Vector3f::UnitX();
            phasespace.systemOrientationX = Eigen::Vector3f::UnitZ();
            phasespace.systemPositionZ = Eigen::Vector3f::UnitX();
            phasespace.systemPositionX = Eigen::Vector3f::UnitZ();
            phasespace.blockLinebreak = "\n";
            phasespace.pointLinebreak = "";
            phasespace.distanceLinebreak = "\n";

            // do analysation of phase space
            analyse_(phasespace, filePotentialMap, fileCoordinates);

            // calculate cluster probability
            probability_(phasespace);
        }

        // two-dimensional case
        if (parameters_.boxDimensions() == 2) {
            // change values of phasespace
            phasespace.stepsPhiOrientation = 1;
            phasespace.deltaThetaOrientation = PI * 2.0 / parameters_.evaluationStepsAngles();
            phasespace.deltaThetaOrientationIncrement = PI;
            phasespace.deltaPhiOrientationIncrement = 0.0;
            phasespace.systemOrientationZ = Eigen::Vector3f::UnitX();
            phasespace.systemOrientationX = Eigen::Vector3f::UnitZ();
            phasespace.systemPositionZ = Eigen::Vector3f::UnitX();
            phasespace.systemPositionX= Eigen::Vector3f::UnitZ();
            phasespace.pointLinebreak = "";

            // do analysation of phase space
            analyse_(phasespace, filePotentialMap, fileCoordinates);

            // calculate cluster probability
            probability_(phasespace);
        }

        // three-dimensional case
        if (parameters_.boxDimensions() == 3) {
          // do analysation of phase space
          analyse_(phasespace, filePotentialMap, fileCoordinates);

          // calculate cluster probability
          probability_(phasespace);
        }
    }

    // generate landscape with perpendicular configurations
    if (evaluationMode == "per") {
        shared::message("janus.evaluate", "generating perpendicular energy landscape");

        // change values of phasespace
        phasespace.stepsThetaOrientation = 1;
        phasespace.stepsPhiOrientation = 1;
        phasespace.deltaDistanceIncrement = 0.5;
        phasespace.deltaThetaOrientationIncrement = 0.0;
        phasespace.deltaPhiOrientationIncrement = 0.0;
        phasespace.blockLinebreak = "";
        phasespace.pointLinebreak = "";
        phasespace.distanceLinebreak = "\n";

        // do analysation of phase space
        analyse_(phasespace, filePotentialMap, fileCoordinates);
    }

    // close output files
    filePotentialMap.close();
    if (parameters_.writeOutput() > 0)
    fileCoordinates.close();
}
