#include "simulation.h"


Simulation::Simulation(const Options& options) try :
    options_(options),
    box_(options),
    step_(0)
{
}
catch (const std::exception& e) {
    std::cerr << "Simulation::Simulation\t" << e.what() << "\n";
    throw;
}


MonteCarlo::MonteCarlo(const Options& options) try :
    Simulation(options),
    steps_output_(options.at("simulation").as<size_t>("output steps")),
    steps_propagation_(options.at("simulation").as<size_t>("propagation steps")),
    steps_simulation_(options.at("simulation").as<size_t>("simulation steps")),
    output_interval_(steps_simulation_ / steps_output_)
{
}
catch (const std::exception& e) {
    std::cerr << "MonteCarlo::MonteCarlo\t" << e.what() << "\n";
    throw;
}


WangLandau::WangLandau(const Options& options) try :
    Simulation(options),
    flatness_target_(0.05),
    number_stages_(20),
    number_substages_(7),
    increment_(1.0),
    interval_(1000),
    interval_base_(interval_),
    stage_(0),
    substage_(0)
{
    populateHistograms_();
    box_.updateClusters();
}
catch (const std::exception& e) {
    std::cerr << "WangLandau::WangLandau\t" << e.what() << "\n";
    throw;
}


void Simulation::attemptRotation_() {
    size_t index = box_.selectParticle();
    box_.saveState(index);
    box_.rotateParticle(index);
    box_.updateEnergy(index);

    if (box_.isAccepted())
        box_.countRotation();
    else
        box_.loadState();
}


void Simulation::attemptTranslation_() {
    size_t index = box_.selectParticle();
    box_.saveState(index);
    box_.translateParticle(index);
    box_.updateCells(index);
    box_.updateEnergy(index);

    if (box_.isAccepted())
        box_.countTranslation();
    else {
        box_.loadState();
        box_.updateCells(index);
    }
}


void Simulation::attemptTranslationClusterupdate_() {
    size_t index = box_.selectParticle();
    box_.saveState(index);
    box_.translateParticle(index);
    box_.updateCells(index);
    box_.updateClusters();
    box_.updateEnergy(index);

    if (box_.isAccepted())
        box_.countTranslation();
    else {
        box_.loadState();
        box_.updateCells(index);
        box_.updateClusters();
    }
}


void Simulation::checkAcceptance_() {
    bool is_check_due = step_ % box_.acceptanceInterval() == 0;
    if (is_check_due) {
        box_.stepCounter(step_);
        box_.updateAcceptance();
    }
}


void MonteCarlo::attemptStep_() {
    checkAcceptance_();
    attemptRotation_();
    attemptTranslation_();
}


void MonteCarlo::run() {
    if (steps_simulation_ > 0 && !options_.count("unrecorded"))
        box_.writeVMD();

    for (step_ = 0; step_ <= steps_propagation_; ++step_) {
        attemptStep_();
        printProgress_(steps_propagation_);
    }

    for (step_ = 0; step_ <= steps_simulation_; ++step_) {
        attemptStep_();
        writeOutput_();
        printProgress_(steps_simulation_);
    }
}


void MonteCarlo::printProgress_(const size_t& step_number) const {
    if (options_.count("progress")) {
        bool is_output_due = step_ % output_interval_ == 0;
        bool is_clear_due = (step_ == steps_simulation_) || (step_ == steps_propagation_);
        if (is_output_due)
            std::cout << "finished step " << step_ << " (" << std::fixed << std::showpoint
                      << std::setprecision(1) << static_cast<float>(step_) / step_number * 100 << '%' << ")\r"
                      << std::flush;
        if (is_clear_due)
            std::cout << "                                        \r" << std::flush;
    }
}


void MonteCarlo::writeOutput_() {
    bool is_output_due = step_ % output_interval_ == 0;
    if (is_output_due && !options_.count("unrecorded")) {
        box_.stepCounter(step_);
        box_.writeGRO();
        box_.writeNRG();
    }
}


void MonteCarloMaximum::attemptStep_() {
    checkAcceptance_();
    attemptRotation_();
    attemptTranslationClusterupdate_();
}


void MonteCarloClusters::run() {
    for (step_ = 0; step_ < steps_propagation_; ++step_) {
        attemptStep_();
        printProgress_(steps_propagation_);
    }

    for (step_ = 0; step_ <= steps_simulation_; ++step_) {
        attemptStep_();
        checkClusters_();
        printProgress_(steps_simulation_);
    }

    printClusters_();
}


void MonteCarloClusters::checkClusters_() {
    bool is_check_due = step_ % output_interval_ == 0 && step_ > 0;
    if (is_check_due) {
        box_.updateClusters();

        for (const auto& cluster : box_.clusters())
            ++cluster_counter[cluster.size()];
    }
}


void MonteCarloClusters::printClusters_() {
    size_t cluster_maximum = 0;
    for (size_t i = 0; i < cluster_counter.size(); ++i)
        if (cluster_counter[i] != 0)
            cluster_maximum = i;

    std::vector<size_t> counter_digits(cluster_maximum+1);
    for (size_t i = 0; i < counter_digits.size(); ++i)
        counter_digits[i] = std::max(util::digitsInNumber(cluster_counter[i]), static_cast<size_t>(6)) + 2;

    // size_t cluster_number_total = std::accumulate(cluster_counter.begin(), cluster_counter.end(), 0);
    // for (size_t i = 1; i <= cluster_maximum; ++i)
    //     std::cout << i
    //               << "\t" << std::setw(9) << cluster_counter[i]
    //               << "\t" << std::setw(6) << std::setprecision(4) << static_cast<float>(cluster_counter[i]) / cluster_number_total
    //               << "\t" << std::setw(6) << std::setprecision(4) << static_cast<float>(cluster_counter[i]) * i / box_.particleNumber() / steps_output_
    //               << "\n";

    size_t cluster_number_total = std::accumulate(cluster_counter.begin(), cluster_counter.end(), 0);
    std::cout.setf(std::ios::fixed|std::ios::showpoint);
    for (size_t i = 1; i <= cluster_maximum; ++i)
        std::cout << std::setw(counter_digits[i]) << i;
    std::cout << "\n";
    for (size_t i = 1; i <= cluster_maximum; ++i)
        std::cout << std::setw(counter_digits[i]) << cluster_counter[i];
    std::cout << "\n";
    for (size_t i = 1; i <= cluster_maximum; ++i)
        std::cout << std::setw(counter_digits[i]) << std::setprecision(4) << static_cast<float>(cluster_counter[i]) / cluster_number_total;
    std::cout << "\n";
    for (size_t i = 1; i <= cluster_maximum; ++i)
        std::cout << std::setw(counter_digits[i]) << std::setprecision(4) << static_cast<float>(cluster_counter[i]) * i / box_.particleNumber() / steps_output_;
    std::cout << "\n";
}


void MonteCarloStates::run() {
    for (step_ = 0; step_ < steps_propagation_; ++step_) {
        attemptStep_();
        printProgress_(steps_propagation_);
    }

    for (step_ = 0; step_ <= steps_simulation_; ++step_) {
        attemptStep_();
        checkStates_();
        printProgress_(steps_simulation_);
    }

    printStates_();
}


void MonteCarloStates::checkStates_() {
    bool is_check_due = step_ % output_interval_ == 0 && step_ > 0;
    if (is_check_due) {
        box_.updateClusters();

        for (const auto& cluster : box_.clusters())
            ++state_counter[cluster.size()];
    }
}


void MonteCarloStates::printStates_() {
    size_t cluster_maximum = 0;
    for (size_t i = 0; i < state_counter.size(); ++i)
        if (state_counter[i] != 0)
            cluster_maximum = i;

    std::vector<size_t> counter_digits(cluster_maximum+1);
    for (size_t i = 0; i < counter_digits.size(); ++i)
        counter_digits[i] = std::max(util::digitsInNumber(state_counter[i]), static_cast<size_t>(6)) + 2;

    size_t cluster_number_total = std::accumulate(state_counter.begin(), state_counter.end(), 0);
    std::cout.setf(std::ios::fixed|std::ios::showpoint);
    for (size_t i = 1; i <= cluster_maximum; ++i)
        std::cout << std::setw(counter_digits[i]) << i;
    std::cout << "\n";
    for (size_t i = 1; i <= cluster_maximum; ++i)
        std::cout << std::setw(counter_digits[i]) << state_counter[i];
    std::cout << "\n";
    for (size_t i = 1; i <= cluster_maximum; ++i)
        std::cout << std::setw(counter_digits[i]) << std::setprecision(4) << static_cast<float>(state_counter[i]) / cluster_number_total;
    std::cout << "\n";
    for (size_t i = 1; i <= cluster_maximum; ++i)
        std::cout << std::setw(counter_digits[i]) << std::setprecision(4) << static_cast<float>(state_counter[i]) * i / box_.particleNumber() / steps_output_;
    std::cout << "\n";
}


bool WangLandau::checkHistograms_() {
    float histograms_sum = 0.0;
    for (const auto& histogram : box_.histograms())
        for (const auto& element : histogram)
            histograms_sum += element.second;
    float histograms_mean = histograms_sum / (box_.histograms()[0].size() + box_.histograms()[1].size());

    bool are_histograms_flat = true;
    for (const auto& histogram : box_.histograms())
        for (const auto& element : histogram) {
            bool is_deviation_okay = (std::abs(element.second / histograms_mean - 1.0) < flatness_target_);
            if (!is_deviation_okay)
                are_histograms_flat = false;
        }

    return are_histograms_flat;
}


void WangLandau::attemptStep_() {
    checkAcceptance_();

    attemptRotation_();
    updateAccumulators_();

    attemptTranslationClusterupdate_();
    updateAccumulators_();
}


void WangLandau::checkStage_() {
    bool is_check_due = step_ % interval_ == 0;
    if (is_check_due && checkHistograms_()) {
        bool is_stage_complete = substage_ == number_substages_;
        if (is_stage_complete) {
            ++stage_;
            substage_ = 0;
            increment_ = increment_ * std::pow(2.0f, number_substages_ - 1);
            interval_ = interval_base_ * std::floor(1.0f / std::sqrt(increment_));
        }
        else {
            ++substage_;
            increment_ /= 2.0f;
        }

        resetHistograms_();
        printProgress_();
    }
}


void WangLandau::evaluate_() {
    for (auto& density : box_.densitiesOfStates())
        for (const auto& element : density)
            if (density.at(element.first) == 0.0)
                density.erase(element.first);

    std::cout << std::fixed << std::showpoint << std::setprecision(4);
    std::cout <<  "state\t\tenergy\tdens (abs)\tdens (rel)\n";
    for (const auto& element : box_.densitiesOfStates()[0])
        std::cout << "unclustered\t" << std::setprecision(1) << std::setw(4) << element.first << "\t"
                                      << std::setprecision(1) << std::setw(6) << element.second << "\t\t"
                                      << std::setprecision(4) << std::setw(7) << 0.0 << "\n";
    for (const auto& element : box_.densitiesOfStates()[1]) {
        float density_of_states = element.second - box_.densitiesOfStates()[0][0.0];
        std::cout << "clustered\t" << std::setprecision(1) << std::setw(4) << element.first << "\t"
                                    << std::setprecision(1) << std::setw(6) << element.second << "\t\t"
                                    << std::setprecision(4) << std::setw(7) << density_of_states << "\n";
    }

    float partition_function = 0.0;
    for (const auto& element : box_.densitiesOfStates()[1])
        partition_function += std::exp(element.second - box_.densitiesOfStates()[0][0.0]) * std::exp(-element.first / box_.temperature());
    float free_energy = -box_.temperature() * std::log(partition_function);

    float partition_function_unclustered = std::log(box_.space() - box_.particleSpace() / box_.particleNumber());
    float free_energy_unclustered = -box_.temperature() * partition_function_unclustered * (box_.particleNumber()-1);

    std::cout << "\nfree energy:\tΔF(" << box_.particleNumber() << ") = " << free_energy << " relative\n";
    std::cout << "\t\tf(" << box_.particleNumber() << ") = " << free_energy + free_energy_unclustered << " absolute\n\n";
}


void WangLandau::populateHistograms_() {
    float energy = util::round(box_.energy(), 1);
    box_.histograms()[0].emplace(0, 0);
    box_.histograms()[1].emplace(energy, 0);
}


void WangLandau::printProgress_() const {
    if (options_.count("progress")) {
        if (stage_ < number_stages_)
            std::cout << "finished stage " << stage_ << "." << substage_ << " of " << number_stages_ << "\r" << std::flush;
        else
            std::cout << "                                  \r" << std::flush;
    }
}


void WangLandau::run() {
    for (step_ = 0; stage_ < number_stages_; ++step_) {
        attemptStep_();
        checkStage_();
    }

    evaluate_();
}
