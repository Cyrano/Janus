#pragma once
#include <algorithm>
#include <string>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include "box/box.h"
#include "interface/options.hpp"


class Phasespace {
    const Options options_;
    Box box_;
    std::string mode_;

    size_t steps_distance_;
    size_t steps_theta_orientation_;
    size_t steps_theta_position_;
    size_t steps_phi_orientation_;
    size_t steps_phi_position_;
    float distance_minimum_;
    float delta_distance_;
    float delta_theta_orientation_;
    float delta_theta_position_;
    float delta_phi_orientation_;
    float delta_phi_position_;
    float delta_distance_increment_;
    float delta_theta_orientation_increment_;
    float delta_theta_position_increment_;
    float delta_phi_orientation_increment_;
    float delta_phi_position_increment_;
    Eigen::Vector3f system_orientation_z_;
    Eigen::Vector3f system_orientation_x_;
    Eigen::Vector3f system_position_z_;
    Eigen::Vector3f system_position_x_;
    std::string linebreak_point_;
    std::string linebreak_block_;
    std::string linebreak_distance_;
    float volume_analytical_;
    float volume_free_;
    float volume_numerical_;
    ac::accumulator_set<float, ac::features<ac::tag::sum>> volume_accumulator_;

    void analyse_ (Phasespace& phasespace, std::ofstream& filePotentialMap, std::ofstream& fileCoordinates);
    void probability_ (Phasespace& phasespace) const;
    size_t stepsMax_() const;
    size_t stepsTotal_() const;

public:
    explicit Phasespace(const Options& options);

    void evaluate(const std::string& fileBasename, const StringVector& filesOutput, const std::string& evaluationMode) const;
};
